#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By : Simon Pageaud
# License    : BSD-3-Clause Copyright 2021, CovDyn Developers.
# =============================================================================
"""
This module contains the GlobalVariables class, a singleton that stores
different data structures to be available anywhere during simulation.
"""
# =============================================================================
# Imports
# =============================================================================
from singleton import Singleton
from datetime import datetime
from vaccine import Vaccine

from math import ceil, floor
import collections


def compute_successive_waves(parameters):
    """
    Computes beta2 to represent consecutive epidemic waves.
    :return:
    """
    beta2_per_iteration_per_age = []
    RO_per_iteration = []
    ts = parameters['time_step']
    sanitary_update = parameters['sanitary_update']
    for section in sanitary_update:
        for i in range(int(sanitary_update[section]['T_b'] / ts), int(sanitary_update[section]['T_e'] / ts)):
            beta2_per_iteration_per_age.append(sanitary_update[section]['beta2_t1'])
            RO_per_iteration.append(sanitary_update[section]['R_0'])
    return beta2_per_iteration_per_age, RO_per_iteration


def compute_new_agents_vaccinated_per_iteration(parameters, population_size_per_age):
    """
    Based on population size and vaccination campaign duration, computes the number of available vaccines doses
    each iteration to have every agent vaccinated at the end of the campaign duration.
    :param parameters:
    :param population_size_per_age: Integer.
    :return: List of first and second doses available per iteration.
    """
    population_size = 0
    vac_prm = parameters['vaccination_parameters']
    # Computes agents based on required age range for vaccine
    vaccine_strategy = vac_prm['vaccine_strategy']
    age_ranges_idx = []
    for index, priority in vac_prm['strategy'][vaccine_strategy]['priority'].items():
        for age in priority['age']:
            age_ranges_idx.append(age)
    for current_age in age_ranges_idx:
        population_size += population_size_per_age[current_age]
    ts = parameters['time_step']
    assert parameters['simulation_duration'] >= vac_prm['campaign_duration']
    available_vaccines = vac_prm['available_vaccines']
    vaccines = {}
    current_vaccine = available_vaccines[0]
    for vaccine in available_vaccines:
        vaccines[vaccine] = Vaccine(vaccine, vac_prm['vaccine_type'][vaccine], ts)
    GlobalVar.Instance().vaccines = vaccines
    t_sec_inj = vaccines[current_vaccine].attributes['iteration_before_next_injection']

    # Apply time step modification
    start = 123
    campaign_duration = vac_prm['campaign_duration'] / ts
    simulation_duration = parameters['simulation_duration'] / ts
    delay = vac_prm['delay'] / ts

    new_agents = ceil(2 * population_size / (campaign_duration - t_sec_inj))
    remaining = 0
    first_shots_per_iteration = []
    second_shots_per_iteration = []
    vaccines_per_iteration = []
    if delay > 0:
        # Linear growth of vaccine per day with specified delay
        missed_agents = delay * new_agents / 2
        missed_agents_per_iteration = ceil(missed_agents / delay)
        new_daily_capacity = 2 * (population_size - missed_agents / 2) / (campaign_duration - t_sec_inj - delay)
        coef = ceil(missed_agents_per_iteration / delay)
        for i in range(int(simulation_duration)):
            if i < start:
                first_shots_per_iteration.append(0)
                vaccines_per_iteration.append([])
            elif i < delay + start - 1:
                first_shots_per_iteration.append((i + 1 - start) * coef)
                vaccines_per_iteration.append(available_vaccines)
            elif delay - 1 + start == i:
                first_shots_per_iteration.append(missed_agents_per_iteration)
                vaccines_per_iteration.append(available_vaccines)
            elif i < campaign_duration - t_sec_inj + start:
                vaccines_per_iteration.append(available_vaccines)
                floored = floor(new_daily_capacity / 2)
                if new_daily_capacity / 2 == floored:
                    first_shots_per_iteration.append(floored)
                else:
                    remaining += new_daily_capacity / 2 - floored
                    first_shots_per_iteration.append(floored + floor(remaining))
                    remaining -= floor(remaining)
            else:
                first_shots_per_iteration.append(0)
            if i < start + t_sec_inj:
                second_shots_per_iteration.append(0)
            elif i < campaign_duration + start:
                vaccines_per_iteration.append(available_vaccines)
                second_shots_per_iteration.append(first_shots_per_iteration[i - int(t_sec_inj)])
            else:
                vaccines_per_iteration.append(available_vaccines)
                second_shots_per_iteration.append(0)
    else:
        # No linear growth (constant through simulation)
        for i in range(int(parameters['simulation_duration'])):
            if i < start:
                vaccines_per_iteration.append([])
                first_shots_per_iteration.append(0)
                second_shots_per_iteration.append(0)
            elif start <= i < t_sec_inj + start:
                vaccines_per_iteration.append(available_vaccines)
                floored = floor(new_agents / 2)
                second_shots_per_iteration.append(0)
                if new_agents / 2 == floored:
                    first_shots_per_iteration.append(floored)
                else:
                    remaining += new_agents / 2 - floored
                    first_shots_per_iteration.append(floored + floor(remaining))
                    remaining -= floor(remaining)
            elif t_sec_inj + start <= i < campaign_duration - t_sec_inj + start:
                vaccines_per_iteration.append(available_vaccines)
                floored = floor(new_agents / 2)
                if new_agents / 2 == floored:
                    first_shots_per_iteration.append(floored)
                    second_shots_per_iteration.append(floored)
                else:
                    remaining += new_agents / 2 - floored
                    first_shots_per_iteration.append(floored + floor(remaining))
                    second_shots_per_iteration.append(floored + floor(remaining))
                    remaining -= floor(remaining)
            elif campaign_duration - t_sec_inj + start <= i < campaign_duration + start:
                vaccines_per_iteration.append(available_vaccines)
                floored = floor(new_agents / 2)
                first_shots_per_iteration.append(0)
                if new_agents / 2 == floored:
                    second_shots_per_iteration.append(floored)
                else:
                    remaining += new_agents / 2 - floored
                    second_shots_per_iteration.append(floored + floor(remaining))
                    remaining -= floor(remaining)
            else:
                vaccines_per_iteration.append([])
                first_shots_per_iteration.append(0)
                second_shots_per_iteration.append(0)
    GlobalVar.Instance().vaccines_per_iteration = vaccines_per_iteration
    return first_shots_per_iteration, second_shots_per_iteration


@Singleton
class GlobalVar(object):
    """
    Stores global attributes such as simulation clock.
    Available to all objects of the simulation.
    Only one instance of GlobalVar is available at anytime.
    """

    def __init__(self):
        self.clock = 0
        self.COVID_model = ""
        self.COVID_model_object = None
        self.beta2 = []
        self.dynamic_beta2 = []
        self.R_per_iteration = []
        self.number_agents_per_compartments = collections.OrderedDict()
        self.number_agents_per_compartments_per_age = collections.OrderedDict()
        self.number_agents_per_compartments_per_age_per_variant = collections.OrderedDict()
        self.time_step = 1
        self.env = None
        self.current_date = datetime.now().isoformat().split('T')[0].replace('-', '')
        self.folder_name = ""
        self.average_global_contacts = 0
        self.already_infected = 0
        self.tracing = False
        self.vaccination = False
        self.average_daily_contacts_infected = []
        self.daily_infected_agents = 0
        self.transition_type = None
        self.vaccinated_agents = 0
        self.vaccinated_agents_per_age = []
        self.capacity_per_iteration_per_age = None
        self.variants = {}
        self.new_agents_vaccination = []
        self.vaccines = {}
        self.first_shot_agents_per_age = []
        self.second_shot_agents_per_age = []
        self.first_shots_per_iteration = []
        self.second_shots_per_iteration = []
        self.success_agents_vaccinated_per_iteration = []
        self.alive_agents_iter = 0
        self.vaccines_per_iteration = []  # List of vaccines names available per iteration
        self.population_of_agents_per_age = []
        self.new_entries_ICU = 0

        # new graphs
        self.agents_in_per_comp_age_variant_iter = {}
        self.agents_out_per_comp_age_variant_iter = {}
        self.vaccines_agents_per_age_iter = {}
        self.cumulated_agents_age_variant = {}

    def update_clock(self):
        self.clock += self.time_step

    def reset(self):
        """
        Resets global variables to their default values.
        """
        self.clock = 0
        self.beta2 = []
        self.dynamic_beta2 = []
        self.R_per_iteration = []
        self.number_agents_per_compartments = collections.OrderedDict()
        self.number_agents_per_compartments_per_age = collections.OrderedDict()
        self.number_agents_per_compartments_per_age_per_variant = collections.OrderedDict()
        self.COVID_model = ""
        self.COVID_model_object = None
        self.time_step = 1
        self.env = None
        self.average_global_contacts = 0
        self.already_infected = 0
        self.tracing = False
        self.vaccination = False
        self.average_daily_contacts_infected = []
        self.daily_infected_agents = 0
        self.transition_type = None
        self.vaccinated_agents = 0
        self.vaccinated_agents_per_age = []
        self.capacity_per_iteration_per_age = None
        self.variants = {}
        self.new_agents_vaccination = []
        self.vaccines = {}
        self.first_shot_agents_per_age = []
        self.second_shot_agents_per_age = []
        self.first_shots_per_iteration = []
        self.second_shots_per_iteration = []
        self.success_agents_vaccinated_per_iteration = []
        self.alive_agents_iter = 0
        self.vaccines_per_iteration = []
        self.population_of_agents_per_age = []
        self.new_entries_ICU = 0
        self.agents_in_per_comp_age_variant_iter = {}
        self.agents_out_per_comp_age_variant_iter = {}
        self.vaccines_agents_per_age_iter = {}
        self.cumulated_agents_age_variant = {}

    def clear_instance(self):
        del self
