#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By : Simon Pageaud
# License    : BSD-3-Clause Copyright 2021, CovDyn Developers.
# =============================================================================
"""
This module contains the variant class that stores its attributes.
"""
# =============================================================================
# Imports
# =============================================================================
from math import floor
import numpy as np
from scipy.special import gamma


class Variant:

    def __init__(self, covid_model, parameters, id_, name, initial_compartments=None):
        """
        Called in seir_model_abstract.py
        :param parameters: dict of values
        :param id_:
        :param name: Variant name
        :param initial_compartments: dict with agents in each compartments at t=0.
        If None, initialize based on incidence rate and real values.
        """
        self.id = id_
        self.name = name

        self.age_dependant_attributes = {}
        self.non_age_dependant_attributes = {}
        self.attributes = {'residence_time': {}}

        # Variant apparition iteration (default value is -1: variant present at the beginning of the simulation)
        self.start = -1

        init_params = self.initialisation_parameters(parameters)

        for attribute, value in parameters['attributes'].items():
            if type(parameters['attributes'][attribute][name]) == list:
                self.age_dependant_attributes[attribute] = value[name]
            else:
                self.non_age_dependant_attributes[attribute] = value[name]
            if 't_' in attribute:
                if parameters['transition_state']['name'] == "Weibull":
                    self.compute_lambda(parameters, attribute, value)
        reduction_factor = parameters['reduction_factor']

        if self.start == -1:
            if not initial_compartments['compartments']:  # if dictionary is empty
                age_distribution_global = parameters['population_per_age']
                age_distribution = []
                for i in range(len(age_distribution_global)):
                    age_distribution.append(floor(reduction_factor * age_distribution_global[i]))

                self.initial_compartment_global = covid_model.create_initial_compartments(
                    age_distribution_global,
                    init_params,
                    self.non_age_dependant_attributes,
                    self.age_dependant_attributes)
            else:
                self.initial_compartment_global = initial_compartments
            self.initial_compartment = self.apply_reduction_factor(reduction_factor)
        else:
            self.initial_compartment = {}

        self.weights_compartments = covid_model.create_residence_time(self.age_dependant_attributes,
                                                                      self.non_age_dependant_attributes,
                                                                      self.attributes['residence_time'])

    def update_attributes_from_parameters(self, parameters):
        """
        Goes through attributes dictionary from the parameters and update age and non age dependant attributes.
        :param parameters: a dictionary of attributes from a json file
        """
        for attribute, value in parameters['attributes'].items():
            if type(parameters['attributes'][attribute][self.name]) == list:
                self.age_dependant_attributes[attribute] = value[self.name]
            else:
                self.non_age_dependant_attributes[attribute] = value[self.name]
            if 't_' in attribute:
                if parameters['transition_state']['name'] == "Weibull":
                    self.compute_lambda(parameters, attribute, value)

    def initialisation_parameters(self, parameters):
        """
        Extracts parameters values from parameter json in 'initial_compartment_per_age'.
        :param parameters:
        :return: a dictionary with extracted values.
        """
        init_attr = {}
        if self.name in parameters['compartments_per_age']:
            params = parameters['compartments_per_age'][self.name]
            details = params['details']
            params_keys = ['percentage_removed', 'agent_per_day', 'percentage_intercept', 'base_variant']
            if "incidence_per_day" in params["details"]:
                init_attr['incidence_rate'], init_attr['agent_per_day'] = (int(x) for x in
                                                                           params["details"]['incidence_per_day'].split(
                                                                               '/'))
            if 'start' in params['details']:
                self.start = params['details']['start']
            for params_key in params_keys:
                if params_key in details:
                    init_attr[params_key] = details[params_key]
            targets = params['targets']
            targets_keys = ['prevalence', 'incidence', 'fill_compartments']
            for targets_key in targets_keys:
                if targets_key in targets:
                    init_attr[targets_key] = targets[targets_key]
        else:
            print('Variant not present in \'initial_compartment_per_age\' from json file.')
        return init_attr

    def apply_reduction_factor(self, reduction_factor):
        """
        Changes population from the total population to reduced population based on reduction factor.
        :return: a dict with reduced population
        """
        initial_compartments = {}
        for compartment_name, individuals in self.initial_compartment_global.items():
            if sum(individuals) < 1 / reduction_factor:
                # Sets population in this compartment to 0 if the number of individuals is inferior to reduction factor
                initial_compartments[compartment_name] = np.zeros(len(individuals), dtype=int).tolist()
            else:
                agents_per_age_decimal = np.asarray(individuals) * reduction_factor
                agents_per_age = np.floor(agents_per_age_decimal)
                rest = agents_per_age_decimal - agents_per_age
                while sum(agents_per_age) < sum(agents_per_age_decimal):
                    max_index = np.where(rest == np.amax(rest))
                    agents_per_age[max_index] += 1
                    rest[max_index] = 0
                initial_compartments[compartment_name] = np.asarray(agents_per_age, dtype=int).tolist()

        return initial_compartments

    def compute_lambda(self, parameters, attribute, value):
        """
        Computes lambda for weibull distribution based on average time in a compartment.
        :param parameters: dict of simulation parameters
        :param attribute: current attribute of a variant (infectiousness, transmission rate, ...)
        :param value: attribute's value (list or float)
        """
        k_factor = parameters['transition_state']['k']
        residence_time_name = 'r_' + attribute
        if type(value[self.name]) == list or type(value[self.name]) == np.ndarray:
            self.attributes['residence_time'][residence_time_name] = []
            for element in value[self.name]:
                self.attributes['residence_time'][residence_time_name].append(element / gamma(1 + 1 / k_factor))
        else:
            self.attributes['residence_time'][residence_time_name] = value[self.name] / gamma(1 + 1 / k_factor)

    def initialize_variant_during_simulation(self, covid_model, parameters, population):
        init_params = self.initialisation_parameters(parameters)
        self.initial_compartment = covid_model.create_initial_compartments_during_simulation(
            init_params['percentage_intercept'], init_params['base_variant'], init_params['fill_compartments']
        )
        covid_model.update_compartments_during_simulation(self, self.initial_compartment, population)
