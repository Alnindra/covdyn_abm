#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By : Simon Pageaud
# License    : BSD-3-Clause Copyright 2021, CovDyn Developers.
# =============================================================================
"""
This module contains the CovDynModel creation and description of
infection and transmission behaviors.
"""
# =============================================================================
# Imports
# =============================================================================
import compartments
from agent import Agent
from global_variables import GlobalVar
from seir_model_abstract import SEIRModel

import pandas as pd
import random as rnd
import numpy as np
from collections import Counter
import math
from scipy.special import gamma


class CovDynModel(SEIRModel):
    def __init__(self, parameters):
        super().__init__(parameters)

        self.attributes = {}
        self.directional_infection = self.draw_again_agents if self.draw_again else self.not_draw_again

        self.symptomatic_compartments = ["paucisymptomatic", "medium_symptoms", "severe_symptoms"]
        self.infected_compartments = ['paucisymptomatic', 'exposed', 'asymptomatic', 'prodromic', 'medium_symptoms',
                                      'severe_symptoms']

    def create_population(self, parameters, population):
        agents = []

        agent_id = 0
        initial_compartment_name = "susceptible"
        compartment_names = parameters['compartment_names']
        for age_index, agents_per_age in enumerate(self.age_distribution, start=0):
            population.susceptible_per_age.append([])
            population.total_susceptible_per_age.append(0)
            population.population_of_agents_per_age.append([])
            population.vaccine_population_per_age.append([])
            for compartment in compartment_names:
                population.number_agents_per_compartments_per_age[compartment].append(0)
                if compartment != initial_compartment_name:
                    for variant_name, variant in GlobalVar.Instance().variants.items():
                        GlobalVar.Instance().number_agents_per_compartments_per_age_per_variant[compartment][
                            variant_name].append(0)
                for variant_name, variant in GlobalVar.Instance().variants.items():
                    GlobalVar.Instance().agents_in_per_comp_age_variant_iter[compartment][
                        variant_name].append(0)
                    GlobalVar.Instance().agents_out_per_comp_age_variant_iter[compartment][
                        variant_name].append(0)
                    if compartment in ['hospitalized', 'ICU']:
                        GlobalVar.Instance().cumulated_agents_age_variant[compartment][variant_name].append(0)
            for local_agent_id in range(0, agents_per_age):
                compartment = compartments.Compartment(initial_compartment_name)
                agent = Agent(agent_id, compartment, population, age_index)
                agent.attributes['contacts'] = self.contact_matrix_per_agent[age_index][local_agent_id]

                self.update_parameters(agent, self.attributes, local_agent_id, age_index)
                self.fetch_parameters_for(agent, parameters, age_index)

                agents.append(agent)
                agent_id += 1
                population.susceptible.append(agent)
                population.population_of_agents.append(agent)
                population.number_agents_per_compartments[initial_compartment_name] += 1
                population.susceptible_per_age[age_index].append(agent)
                population.total_susceptible_per_age[age_index] += 1
                population.number_agents_per_compartments_per_age[initial_compartment_name][age_index] += 1
                population.population_of_agents_per_age[age_index].append(agent)
                if GlobalVar.Instance().vaccination and agent.attributes['wants_vaccine']:
                    # Adds only agents who want to be vaccinated
                    population.vaccine_population_per_age[age_index].append(agent)
        return agents

    def update_contacts(self, population):
        agent_counter = 0
        for age_index, agents_per_age in enumerate(self.age_distribution, start=0):
            for local_agent_id in range(0, agents_per_age):
                assert population.agents[agent_counter].age_id == age_index
                population.agents[agent_counter].attributes['contacts'] = self.contact_matrix_per_agent[age_index][
                    local_agent_id]
                agent_counter += 1

    def update_variants_parameters(self, population):
        for agent in population.agents:
            if agent.variant_name is not None:
                self.fetch_variant_parameters(agent, self.variants[agent.variant_name], agent.age_id)

    @staticmethod
    def update_compartment(agent, compartment_name, clock):
        """
        Used for the reset of agents in calibration settings
        """
        agent.compartment = compartments.Compartment(compartment_name, agent.attributes)
        agent.compartment.current_state.time = clock

    def create_compartments(self, parameters, population):
        compartment_names = parameters['compartment_names']
        compartments_v2 = False
        if compartments_v2:
            compartment_names.append('hospitalizedSecond')
            compartment_names.append('hospitalizedThird')
        for variant_name, variant in self.variants.items():
            for compartment_name, number_per_age in variant.initial_compartment.items():
                if compartment_name in compartment_names:
                    for index, value in enumerate(number_per_age):
                        agents_per_age = rnd.sample(population.susceptible_per_age[index], k=value)

                        # Update initial residence time (No age range yet (Hosp, ICU))
                        agents_per_clock = []
                        if compartment_name in variant.weights_compartments:
                            # To avoid compartments like Deceased and Removed
                            clocks_per_compartment_age = variant.weights_compartments[compartment_name]
                            agents_per_clock = []
                            if type(clocks_per_compartment_age[0]) == list:
                                # age range based clock
                                for weight in clocks_per_compartment_age[index]:
                                    agents_per_clock.append(math.floor(weight * len(agents_per_age)))

                                agents_per_clock = self.stronger_stays(len(agents_per_age),
                                                                       clocks_per_compartment_age[index],
                                                                       agents_per_clock)
                            else:
                                # no age range
                                for weight in clocks_per_compartment_age:
                                    agents_per_clock.append(math.floor(weight * len(agents_per_age)))
                                agents_per_clock = self.stronger_stays(len(agents_per_age),
                                                                       clocks_per_compartment_age,
                                                                       agents_per_clock)

                            assert len(agents_per_age) - sum(agents_per_clock) == 0

                        for agent in agents_per_age:
                            self.fetch_variant_parameters(agent, variant, agent.age_id)

                            agent.compartment = compartments.Compartment(compartment_name, agent.attributes)
                            if len(agents_per_clock) > 0:
                                for residence_time, avail_clock in enumerate(agents_per_clock):
                                    if avail_clock > 0:
                                        agent.compartment.current_state.time = residence_time
                                        agents_per_clock[residence_time] -= 1
                                        break

                            GlobalVar.Instance().number_agents_per_compartments_per_age_per_variant[compartment_name][
                                variant_name][index] += 1
                            population.number_agents_per_compartments_per_age[compartment_name][index] += 1
                            population.number_agents_per_compartments_per_age['susceptible'][index] -= 1
                            population.number_agents_per_compartments[compartment_name] += 1
                            population.number_agents_per_compartments['susceptible'] -= 1

                            population.susceptible_per_age[index].remove(agent)

    def create_columns_for_csv(self):
        columns_name = ['S', 'E', 'Ip', 'A', 'Ips', 'Ims', 'Iss', 'H1', 'H2', 'H3', 'ICU', 'R', 'D']
        age_ranges = ['0-4', '5-9', '10-14', '15-19', '20-24',
                      '25-29', '30-34', '35-39', '40-44', '45-49',
                      '50-54', '55-59', '60-64', '65-69', '70-74',
                      '75-79', '80P']
        first_header = ['']
        second_header = ['']

        if len(GlobalVar.Instance().variants) > 1:
            # Susceptible population has no variant
            columns_name_ = columns_name.copy()
            for age in age_ranges:
                first_header.append(columns_name_[0])
                second_header.append(age)
            columns_name_.pop(0)

            for name in columns_name_:
                for variant_name, variant_attributes in GlobalVar.Instance().variants.items():
                    for age in age_ranges:
                        first_header.append(name + '_' + variant_name)
                        second_header.append(age)
            return pd.MultiIndex.from_tuples(zip(first_header, second_header))

        for column in columns_name:
            for age in age_ranges:
                first_header.append(column)
                second_header.append(age)
        return pd.MultiIndex.from_tuples(zip(first_header, second_header))

    def directional_infection(self, agent, infection_probability, contacts):
        newly_infected_agents, contacts_without_infection = [], []
        remaining_contacts = agent.attributes['contacts'] - len(contacts)
        if remaining_contacts > 0:
            # Draw and sort contacts per age range
            contacts_age = rnd.sample(self.proportions_of_contacts_per_age[agent.age_id], remaining_contacts)
            contacts_age_sorted = Counter(contacts_age)
            for age_range, agents_from_this_range in contacts_age_sorted.items():
                local_remaining_contacts = agents_from_this_range
                ids_already_drawn = [agent.id]  # Adds the current agent id
                ids_already_drawn.extend(contacts)  # Adds its current contacts for the iteration
                while local_remaining_contacts > 0:
                    current_contact = rnd.choice(GlobalVar.Instance().population_of_agents_per_age[age_range])
                    if current_contact.id not in ids_already_drawn:
                        ids_already_drawn.append(current_contact.id)
                        if rnd.random() < infection_probability:
                            # Infectious contact
                            if self.draw_again:
                                correct_infection = self.check_multiple_infections_and_store(current_contact,
                                                                                             newly_infected_agents,
                                                                                             contacts_without_infection)
                                # If there is no infection, we start again until we have a correct infection
                                while correct_infection == 0:
                                    current_contact = rnd.choice(
                                        GlobalVar.Instance().population_of_agents_per_age[age_range])
                                    if current_contact.id not in ids_already_drawn:
                                        # If its an infectious contact, check if the agent was already infected
                                        ids_already_drawn.append(current_contact.id)
                                        if rnd.random() < infection_probability:
                                            # Infectious contact
                                            correct_infection = self.check_multiple_infections_and_store(
                                                current_contact, newly_infected_agents, contacts_without_infection)
                                        else:
                                            contacts_without_infection.append(current_contact)
                                            correct_infection -= 1
                                local_remaining_contacts -= 1
                            else:
                                newly_infected_agents.append(current_contact)
                                local_remaining_contacts -= 1
                        else:
                            contacts_without_infection.append(current_contact)
                            local_remaining_contacts -= 1
        return newly_infected_agents, contacts_without_infection

    def not_draw_again(self, agent, infection_probability, contacts):
        """
        Version with embedded function
        :param agent:
        :param infection_probability:
        :param contacts:
        :return:
        """
        newly_infected_agents, contacts_without_infection = [], []
        remaining_contacts = agent.attributes['contacts'] - len(contacts)
        if remaining_contacts > 0:
            # Draw and sort contacts per age range
            contacts_age = rnd.sample(self.proportions_of_contacts_per_age[agent.age_id], remaining_contacts)
            contacts_age_sorted = Counter(contacts_age)
            for age_range, agents_from_this_range in contacts_age_sorted.items():
                local_remaining_contacts = agents_from_this_range
                ids_already_drawn = [agent.id]  # Adds the current agent id
                ids_already_drawn.extend(contacts)  # Adds its current contacts for the iteration
                while local_remaining_contacts > 0:
                    current_contact = rnd.choice(GlobalVar.Instance().population_of_agents_per_age[age_range])
                    if current_contact.id not in ids_already_drawn:
                        ids_already_drawn.append(current_contact.id)
                        if rnd.random() < infection_probability:  # infection_probability_updated:
                            # Infectious contact
                            newly_infected_agents.append(current_contact)
                            local_remaining_contacts -= 1
                        else:
                            contacts_without_infection.append(current_contact)
                            local_remaining_contacts -= 1
        return newly_infected_agents, contacts_without_infection

    def draw_again_agents(self, agent, infection_probability, contacts):
        newly_infected_agents, contacts_without_infection = [], []
        remaining_contacts = agent.attributes['contacts'] - len(contacts)
        if remaining_contacts > 0:
            # Draw and sort contacts per age range
            contacts_age = rnd.sample(self.proportions_of_contacts_per_age[agent.age_id], remaining_contacts)
            contacts_age_sorted = Counter(contacts_age)
            for age_range, agents_from_this_range in contacts_age_sorted.items():
                local_remaining_contacts = agents_from_this_range
                ids_already_drawn = [agent.id]  # Adds the current agent id
                ids_already_drawn.extend(contacts)  # Adds its current contacts for the iteration
                while local_remaining_contacts > 0:
                    current_contact = rnd.choice(GlobalVar.Instance().population_of_agents_per_age[age_range])
                    if current_contact.id not in ids_already_drawn:
                        ids_already_drawn.append(current_contact.id)
                        if rnd.random() < infection_probability:  # infection_probability_updated:
                            # Infectious contact
                            correct_infection = self.check_multiple_infections_and_store(current_contact,
                                                                                         newly_infected_agents,
                                                                                         contacts_without_infection)
                            # If there is no infection, we start again until we have a correct infection
                            while correct_infection == 0:
                                current_contact = rnd.choice(
                                    GlobalVar.Instance().population_of_agents_per_age[age_range])
                                if current_contact.id not in ids_already_drawn:
                                    # If its an infectious contact, check if the agent was already infected
                                    ids_already_drawn.append(current_contact.id)
                                    if rnd.random() < infection_probability:  # infection_probability_updated:
                                        # Infectious contact
                                        correct_infection = self.check_multiple_infections_and_store(
                                            current_contact, newly_infected_agents, contacts_without_infection)
                                    else:
                                        contacts_without_infection.append(current_contact)
                                        correct_infection -= 1
                            local_remaining_contacts -= 1
                        else:
                            contacts_without_infection.append(current_contact)
                            local_remaining_contacts -= 1
        return newly_infected_agents, contacts_without_infection

    def directional_infection2(self, agent, infection_probability, contacts):
        """
        Version with inward and outward efficiency
        :param agent:
        :param infection_probability:
        :param contacts:
        :return:
        """
        newly_infected_agents, contacts_without_infection = [], []
        remaining_contacts = agent.attributes['contacts'] - len(contacts)
        if remaining_contacts > 0:
            # Draw and sort contacts per age range
            contacts_age = rnd.sample(self.proportions_of_contacts_per_age[agent.age_id], remaining_contacts)
            contacts_age_sorted = Counter(contacts_age)
            for age_range, agents_from_this_range in contacts_age_sorted.items():
                local_remaining_contacts = agents_from_this_range
                ids_already_drawn = [agent.id]  # Adds the current agent id
                ids_already_drawn.extend(contacts)  # Adds its current contacts for the iteration
                while local_remaining_contacts > 0:
                    current_contact = rnd.choice(GlobalVar.Instance().population_of_agents_per_age[age_range])
                    if current_contact.id not in ids_already_drawn:

                        ids_already_drawn.append(current_contact.id)
                        infection_probability_updated = self.infection_probability_update(infection_probability,
                                                                                          agent, current_contact)
                        if rnd.random() < infection_probability_updated:
                            # Infectious contact
                            if self.draw_again:
                                correct_infection = self.check_multiple_infections_and_store(current_contact,
                                                                                             newly_infected_agents,
                                                                                             contacts_without_infection)
                                # If there is no infection, we start again until we have a correct infection
                                while correct_infection == 0:
                                    current_contact = rnd.choice(
                                        GlobalVar.Instance().population_of_agents_per_age[age_range])
                                    if current_contact.id not in ids_already_drawn:
                                        # If its an infectious contact, check if the agent was already infected
                                        ids_already_drawn.append(current_contact.id)
                                        infection_probability_updated = self.infection_probability_update(
                                            infection_probability,
                                            agent, current_contact)
                                        if rnd.random() < infection_probability_updated:
                                            # Infectious contact
                                            correct_infection = self.check_multiple_infections_and_store(
                                                current_contact, newly_infected_agents, contacts_without_infection)
                                        else:
                                            contacts_without_infection.append(current_contact)
                                            correct_infection -= 1
                                local_remaining_contacts -= 1
                            else:
                                newly_infected_agents.append(current_contact)
                                local_remaining_contacts -= 1
                        else:
                            contacts_without_infection.append(current_contact)
                            local_remaining_contacts -= 1
        return newly_infected_agents, contacts_without_infection

    def draw_contacts_for_agent(self, agent, number_of_potential_new_infection):
        newly_infected_agents = []
        contacts_age = rnd.sample(self.proportions_of_contacts_per_age[agent.age_id],
                                  number_of_potential_new_infection)
        contacts_age_sorted = Counter(contacts_age)
        if self.draw_again:  # Draw another agent if the agent was infected in this turn
            for age_range, agents_from_this_range in contacts_age_sorted.items():
                local_remaining_infection_to_perform = agents_from_this_range
                ids_already_drawn = [agent.id]
                while local_remaining_infection_to_perform > 0:
                    newly_infected_agent = rnd.choice(GlobalVar.Instance().population_of_agents_per_age[age_range])
                    if newly_infected_agent.id not in ids_already_drawn:
                        ids_already_drawn.append(newly_infected_agent.id)
                        local_remaining_infection_to_perform += self.check_multiple_infections(newly_infected_agent,
                                                                                               newly_infected_agents)
                    else:  # this agent was already drawn
                        pass
        else:
            for age_range, agents_from_this_range in contacts_age_sorted.items():
                newly_infected_agents.extend(rnd.sample(GlobalVar.Instance().population_of_agents_per_age[age_range],
                                                        agents_from_this_range))
        return newly_infected_agents

    def live(self, file_name=None):
        values = []
        if file_name == 'cumulative_hospital':
            for compartment, variants in GlobalVar.Instance().cumulated_agents_age_variant.items():
                for variant, ages in variants.items():
                    for agents_in_age_range in ages:
                        values.append(agents_in_age_range)
            return values
        if GlobalVar.Instance().vaccination:
            if file_name == 'vaccines':
                values = GlobalVar.Instance().first_shot_agents_per_age + GlobalVar.Instance().\
                    second_shot_agents_per_age
                return values
        if file_name == 'ins':
            for compartment, variants in GlobalVar.Instance().agents_in_per_comp_age_variant_iter.items():
                for variant, ages in variants.items():
                    for agents_in_age_range in ages:
                        values.append(agents_in_age_range)
            return values
        if file_name == 'outs':
            for compartment, variants in GlobalVar.Instance().agents_out_per_comp_age_variant_iter.items():
                for variant, ages in variants.items():
                    for agents_in_age_range in ages:
                        values.append(agents_in_age_range)
            return values
        else:
            pass
        values2 = []
        if len(GlobalVar.Instance().variants) == 1:
            values = [item[1] for item in GlobalVar.Instance().number_agents_per_compartments_per_age.items()]

            for value in values:
                for element in value:
                    values2.append(element)
        else:
            # Susceptible population has variants
            for number_agents_per_age in GlobalVar.Instance().number_agents_per_compartments_per_age['susceptible']:
                values2.append(number_agents_per_age)
            for compartment_name, variants in GlobalVar.Instance().\
                    number_agents_per_compartments_per_age_per_variant.items():
                for variant_name, age_ranges in variants.items():
                    for agents_in_this_age_range in age_ranges:
                        values2.append(agents_in_this_age_range)

        return values2

    @staticmethod
    def state_machine_transitions(non_age_dependant_attributes, age_dependant_attributes, age_distribution):
        """
        Describes the average time spend per compartment and the probability to change compartment.
        :param non_age_dependant_attributes:
        :param age_dependant_attributes:
        :param age_distribution:
        :return: a dict of average time per compartments and a dict of probability to change compartment.
        """
        average_time_per_compartment = {
            "exposed": (non_age_dependant_attributes["t_i"] - non_age_dependant_attributes["t_p"]) * np.ones(
                len(age_distribution)),
            "prodromic": non_age_dependant_attributes["t_p"] * np.ones(len(age_distribution)),
            "asymptomatic": non_age_dependant_attributes["t_s"] * np.ones(len(age_distribution)),
            "paucisymptomatic": non_age_dependant_attributes["t_s"] * np.ones(len(age_distribution)),
            "medium_symptoms": non_age_dependant_attributes["t_s"] * np.ones(len(age_distribution)),
            "severe_symptoms": np.ones(len(age_distribution)),
            "hospitalized": age_dependant_attributes['t_H'],
            "ICU": age_dependant_attributes['t_ICU']}
        proba_change_compartment = {"exposed": 1 * np.ones(len(age_distribution)),
                                    "prodromic": 1 * np.ones(len(age_distribution)),
                                    "asymptomatic": non_age_dependant_attributes["p_A"] * np.ones(
                                        len(age_distribution)),
                                    "paucisymptomatic": np.asarray(age_dependant_attributes["p_Ips"]) * (
                                            1 - non_age_dependant_attributes["p_A"]),
                                    "medium_symptoms": np.asarray(age_dependant_attributes["p_Ims"]) * (
                                            1 - non_age_dependant_attributes["p_A"]),
                                    "severe_symptoms": np.asarray(age_dependant_attributes["p_H"]) * (
                                            1 - non_age_dependant_attributes["p_A"]),
                                    "hospitalized": 1 * np.ones(len(age_distribution)),
                                    "ICU": np.ones(len(age_distribution)) * age_dependant_attributes[
                                        'p_ICU']}
        return average_time_per_compartment, proba_change_compartment

    def create_initial_compartments(self, age_distribution, initialization_values, non_age_dependant_attributes,
                                    age_dependant_attributes):
        """
        Computes the number of agents per compartment per age range at the beginning of the simulation
        :param age_distribution:
        :param initialization_values: dict of values from json parameters file
        :param non_age_dependant_attributes:
        :param age_dependant_attributes:
        :return: dict of compartments with list of agents per age range
        """
        updated_compartments = initialization_values['fill_compartments'].copy()
        initial_compartments = {}

        filled_compartments = []  # Stores the name of compartments already filled
        average_time_per_compartment, proba_change_compartment = self.state_machine_transitions(
            non_age_dependant_attributes,
            age_dependant_attributes,
            age_distribution)

        # If prevalence is available in 'initial_compartments_per_age:targets'
        if 'prevalence' in initialization_values:
            for compartment_name, individuals_per_age in initialization_values['prevalence'].items():
                initial_compartments[compartment_name] = individuals_per_age
                filled_compartments.append(compartment_name)

        compartments_v2 = False
        if compartments_v2:
            # Splits hospitalized evenly into H1, H2 and H3
            if 'hospitalized' in initial_compartments:
                hospitalized = np.asarray(initial_compartments['hospitalized'])
                rest, hospitalized_split = np.modf(hospitalized / 3)
                initial_compartments['hospitalized'] = hospitalized_split + np.where(rest > 0, 1, rest)
                initial_compartments['hospitalizedSecond'] = hospitalized_split + np.where(
                    rest < 0.5, 0, np.where(rest > 0.5, 1, rest))
                initial_compartments['hospitalizedThird'] = hospitalized_split

        # If incidence is available in 'initial_compartments_per_age:targets' then do a reverse fill
        if 'incidence' in initialization_values:
            incidence_compartments = {}
            previous_compartments = {"hospitalized": ['severe_symptoms'],
                                     "severe_symptoms": ['prodromic'],
                                     'prodromic': ['exposed']}
            for compartment_name, individuals_per_age in initialization_values['incidence'].items():
                incidence_compartments[compartment_name] = individuals_per_age
            for compartment_name, incidence_per_age in incidence_compartments.items():
                for previous_compartment_name in previous_compartments[compartment_name]:
                    initial_compartments[previous_compartment_name] = (np.floor((np.asarray(incidence_per_age) *
                                                                       average_time_per_compartment[
                                                                           previous_compartment_name]) /
                                                                                proba_change_compartment[
                                                                          compartment_name])).tolist()

                    filled_compartments.append(previous_compartment_name)
            for filled_compartment in filled_compartments:
                if filled_compartment in previous_compartments:
                    for previous_compartment in previous_compartments[filled_compartment]:
                        if previous_compartment not in filled_compartments:
                            initial_compartments[previous_compartment] = (np.floor(
                                (initial_compartments[filled_compartment] *
                                 average_time_per_compartment[previous_compartment] /
                                 proba_change_compartment[filled_compartment]))).tolist()
                            filled_compartments.append(previous_compartment)

            # For the remaining compartments, do a forward fill
            previous_compartments_2 = {'asymptomatic': ['prodromic'],
                                       'paucisymptomatic': ['prodromic'],
                                       'medium_symptoms': ['prodromic']}
            for compartment_to_be_filled in updated_compartments:
                if compartment_to_be_filled not in filled_compartments:
                    for previous_compartment in previous_compartments_2[compartment_to_be_filled]:
                        initial_compartments[compartment_to_be_filled] = (
                            np.floor(np.asarray(initial_compartments[previous_compartment]) *
                                     proba_change_compartment[compartment_to_be_filled] /
                                     average_time_per_compartment[previous_compartment])).tolist()
                        filled_compartments.append(compartment_to_be_filled)
        if 'percentage_intercept' in initialization_values:
            for compartment_to_be_filled in initialization_values['fill_compartments']:
                initial_compartments[compartment_to_be_filled] = np.floor(
                    initialization_values['percentage_intercept'] *
                    np.asarray(GlobalVar.Instance().variants['regular'].initial_compartment_global[
                                   compartment_to_be_filled])).tolist()

        if 'percentage_removed' in initialization_values:
            initial_compartments['removed'] = np.floor(np.asarray(age_distribution) * initialization_values[
                'percentage_removed']).tolist()
        return initial_compartments

    def create_initial_compartments_during_simulation(self, intercept, base_variant, compartments_to_fill):
        initial_compartments = {}
        for compartment_to_be_filled in compartments_to_fill:
            rest, agents = np.modf(intercept * np.asarray(
                GlobalVar.Instance().number_agents_per_compartments_per_age_per_variant[
                    compartment_to_be_filled][base_variant]))
            total = sum(rest + agents)
            while sum(agents) < total:
                max_index = np.where(rest == np.amax(rest))
                agents[max_index] += 1
                rest[max_index] = 0
            initial_compartments[compartment_to_be_filled] = agents.astype(int).tolist()
        return initial_compartments

    def update_compartments_during_simulation(self, current_variant, initial_compartments, population):
        for variant_name, variant in self.variants.items():
            if current_variant.name == variant_name:
                for compartment_name, number_per_age in initial_compartments.items():
                    for index, value in enumerate(number_per_age):
                        agents_per_age = rnd.sample(population.susceptible_per_age[index], k=value)
                        for agent in agents_per_age:
                            self.fetch_variant_parameters(agent, variant, agent.age_id)
                            # Add environment to agent
                            agent.add_environment(population.env)
                            agent.action = agent.env.process(agent.live())

                            agent.compartment = compartments.Compartment(compartment_name, agent.attributes)

                            GlobalVar.Instance().number_agents_per_compartments_per_age_per_variant[compartment_name][
                                variant_name][index] += 1
                            population.number_agents_per_compartments_per_age[compartment_name][index] += 1
                            population.number_agents_per_compartments_per_age['susceptible'][index] -= 1
                            population.number_agents_per_compartments[compartment_name] += 1
                            population.number_agents_per_compartments['susceptible'] -= 1

                            population.susceptible_per_age[index].remove(agent)

    def create_residence_time(self, age_dependant_attributes, non_age_dependant_attributes, residence_time_attributes):
        """
        Computes the time already spent by agents in their initial compartment per age range based
        on their residence time.
        :return:
        """
        weights_per_compartment_age = {}
        time_step = 1
        weibull_k = 10
        age_ranges = 17
        compartments_with_age_range = ['hospitalized', 'ICU']
        compartments = ['exposed', 'asymptomatic', 'prodromic', 'paucisymptomatic',
                        'medium_symptoms', 'severe_symptoms', 'hospitalized', 'ICU']
        compartment_time = {
            'exposed': non_age_dependant_attributes['t_i'] - non_age_dependant_attributes['t_p'],
            'prodromic': non_age_dependant_attributes['t_p'],
            'asymptomatic': non_age_dependant_attributes['t_s'],
            'paucisymptomatic': non_age_dependant_attributes['t_s'],
            'medium_symptoms': non_age_dependant_attributes['t_s'],
            'severe_symptoms': non_age_dependant_attributes['t_bh'],
            'hospitalized': age_dependant_attributes['t_H'],
            'ICU': age_dependant_attributes['t_ICU']}
        assert non_age_dependant_attributes['t_i'] == 5.1
        assert non_age_dependant_attributes['t_p'] == 1.5
        # If the previous assert fails, we need to implement a dynamic age attribute identification
        mean_res_times = {
            'exposed': (non_age_dependant_attributes['t_i'] - non_age_dependant_attributes['t_p']) / gamma(
                1 + 1 / weibull_k),
            'prodromic': residence_time_attributes['r_t_p'],
            'asymptomatic': residence_time_attributes['r_t_s'],
            'paucisymptomatic': residence_time_attributes['r_t_s'],
            'medium_symptoms': residence_time_attributes['r_t_s'],
            'severe_symptoms': residence_time_attributes['r_t_bh'],
            'hospitalized': residence_time_attributes['r_t_H'],
            'ICU': residence_time_attributes['r_t_ICU']}
        for compartment in compartments:
            if compartment in compartments_with_age_range:
                weight_clock_age = []
                for index_age_range in range(age_ranges):
                    proba_stay = []
                    proba_survie = []
                    weight_clock = []
                    mean_res_time = mean_res_times[compartment][index_age_range]
                    mean_time_in_comp = compartment_time[compartment][index_age_range]
                    for i in range(2 * math.ceil(mean_time_in_comp * 1 / time_step)):
                        proba_stay.append(self.weibull_distribution(i, time_step, mean_res_time, weibull_k=weibull_k))
                        sum_temp = 1
                        for j in range(len(proba_stay)):
                            sum_temp *= (1 - proba_stay[j])
                        proba_survie.append(sum_temp)
                        if proba_stay[-1] >= 1:
                            # Useless to compute after that because no agents can stay longer in this compartment
                            break
                    for proba_survie_t in proba_survie:
                        weight_clock.append(proba_survie_t / sum(proba_survie))
                    weight_clock_age.append(weight_clock)
                weights_per_compartment_age[compartment] = weight_clock_age
            else:
                proba_stay = []
                proba_survie = []
                weight_clock = []
                mean_res_time = mean_res_times[compartment]
                mean_time_in_comp = compartment_time[compartment]
                for i in range(2 * math.ceil(mean_time_in_comp * 1 / time_step)):
                    # computes probability to stay in compartment for i iterations
                    proba_stay.append(self.weibull_distribution(i, time_step, mean_res_time, weibull_k=weibull_k))
                    sum_temp = 1
                    for j in range(len(proba_stay)):
                        sum_temp *= (1 - proba_stay[j])
                    proba_survie.append(sum_temp)
                    if proba_stay[-1] >= 1:
                        # Useless to compute after that because no agents can stay longer in this compartment
                        break
                for proba_survie_t in proba_survie:
                    weight_clock.append(proba_survie_t / sum(proba_survie))
                weights_per_compartment_age[compartment] = weight_clock
        for age_range in range(age_ranges):
            pass
        return weights_per_compartment_age

    @staticmethod
    def weibull_distribution(cur_res_time, time_step, mean_res_time, weibull_k):
        """

        :param weibull_k:
        :param mean_res_time:
        :param time_step:
        :param cur_res_time:
        :return:
        """
        return 1 - math.exp(math.pow((cur_res_time - 0.5 * time_step) / mean_res_time, weibull_k) -
                            math.pow((cur_res_time + 0.5 * time_step) / mean_res_time, weibull_k))

    @staticmethod
    def stronger_stays(total, weights, current_list):
        """

        :param total:
        :param weights:
        :param current_list:
        :return:
        """
        rest = []
        for weight in weights:
            rest.append((total * weight) - math.floor(total * weight))
        while sum(current_list) < total:
            max_index = rest.index(max(rest))
            current_list[max_index] += 1
            rest[max_index] = 0
        return current_list
