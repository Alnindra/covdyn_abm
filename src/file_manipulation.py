#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By : Simon Pageaud
# License    : BSD-3-Clause Copyright 2021, CovDyn Developers.
# =============================================================================
"""
This module contains the functions to create, read and write files.
"""
# =============================================================================
# Imports
# =============================================================================
from global_variables import GlobalVar

import json
import pandas as pd
import pandas
import os

path = '../results/'


def write_to_file(folder_name, filename, line):
    f = open(path + folder_name + filename, "a")
    f.write(line)
    f.close()


def load_json_file(filename):
    return json.load(open(filename))


def save_json_file(file_path, json_file):
    with open(file_path, 'w') as f:
        json.dump(json_file, f)


def parse_parameters_file(filename):
    data = load_json_file(filename)
    model_parameters = {}
    for simulation_parameter in data['simulation']:
        model_parameters[simulation_parameter] = data['simulation'][simulation_parameter]

    # Covid-19 related attributes
    model_parameters['COVID_model'] = data['simulation']['COVID_model']
    COVID_model = model_parameters['COVID_model']
    for parameter in data['COVID']['model'][COVID_model]:
        model_parameters[parameter] = data['COVID']['model'][COVID_model][parameter]
    model_parameters['compartments'] = data['COVID']['initial_compartment']
    if 'initial_compartment_per_age' in data['COVID']:
        model_parameters['compartments_per_age'] = data['COVID']['initial_compartment_per_age']
    if 'lockdown' in data['COVID']['model'][COVID_model]:
        for parameter in data['COVID']['model'][COVID_model]['lockdown']:
            model_parameters[parameter] = data['COVID']['model'][COVID_model]['lockdown'][parameter]
    if 'infection_only_susceptible' in data['COVID']['details']:
        model_parameters['infection_only_susceptible'] = data['COVID']['details']['infection_only_susceptible']
    if 'infection_type' in data['COVID']['details']:
        model_parameters['infection_type'] = data['COVID']['details']['infection_type']
    if 'distribution' in data['COVID']['details']:
        model_parameters['transition_state'] = {'name': data['COVID']['details']['distribution']['type']}
        type_probability = data['COVID']['details']['distribution']['type']
        for element in data['COVID']['details']['distribution'][type_probability]:
            model_parameters['transition_state'][element] = data['COVID']['details']['distribution'][type_probability][element]
    if 'R0_base4dynamic_Re' in data['COVID']['details']:
        model_parameters['R0_base4dynamic_Re'] = data['COVID']['details']['R0_base4dynamic_Re']
    if "attributes_update" in data['COVID']:
        model_parameters["attributes_update"] = data['COVID']['attributes_update']

    # Global simulation attributes
    if 'contact_matrix' in data['population']:
        for parameter in data['population']['contact_matrix']:
            model_parameters[parameter] = data['population']['contact_matrix'][parameter]
    if 'population_distribution' in data['population']:
        for parameter in data['population']['population_distribution']:
            model_parameters[parameter] = data['population']['population_distribution'][parameter]

    # Tracing related attributes
    tracing = 'tracing'
    parse_attributes_for(tracing, data, model_parameters)

    # Testing related attributes
    testing = 'testing'
    parse_attributes_for(testing, data, model_parameters)

    # Masking related attributes
    masking = 'masking'
    parse_attributes_for(masking, data, model_parameters)

    # Vaccine related attributes
    vaccination = 'vaccination'
    parse_attributes_for(vaccination, data, model_parameters)

    return model_parameters


def parse_attributes_for(name, data, parameters):
    """
    Parse the named block in the json file.
    :param name: Name of the field in the json
    :param data: Loaded json
    :param parameters: Dict of stored attributed
    """
    name_parameters = name + '_parameters'
    if name in data:
        parameters[name] = check_availability(data, name, 'available')
        parameters[name_parameters] = {}
        if parameters[name]:
            for parameter in data[name]:
                parameters[name_parameters][parameter] = data[name][parameter]


def check_availability(parsed_json, parameter_name, field_name):
    if parsed_json[parameter_name][field_name]:
        return True
    return False


def write_to_csv(filename, columns, data):
    df = pd.DataFrame(data, columns=columns)
    df.to_csv(filename)


def read_csv(filename):
    df = pandas.read_csv(filename, sep=',')
    return df


def write_infections(folder_name, filename, data):
    """

    :param folder_name:
    :param filename:
    :param data: dict with infectious agent id and their contacts for each day of the simulation
    :return:
    """
    for time_step, ids in data.items():
        line = str(time_step) + ')'
        for agent_id, values in ids.items():
            new_line = str(agent_id) + ':'
            for key, element in values.items():
                if key == "comp":
                    new_line += str(element) + ','
                else:
                    new_line += str(key) + str(element) + ','
            new_line = ";".join(new_line.rsplit(',', 1))
            line += new_line
        line += '\n'
        write_to_file(folder_name, filename, line)


def update_json(json_file, new_parameters):
    """

    :param json_file:
    :param new_parameters: Dictionary with the full parameter name separated by ':' as key and updated value as value.
    For example: if we have a json file with the following structure {'a':{'b':c,'d':{'e':f}}} and we want to change
    parameter 'e' from f to h then new_parameters = {'a:d:e':h}
    :return:
    """
    for field, value in new_parameters.items():
        fields_name = field.split(':')
        deepness = 0
        find_parameter_and_update(fields_name, deepness, json_file, value)


def find_parameter_and_update(fields_name, deepness, dictionary, updated_value):
    """
    Recursive function that goes inside nested dictionaries and update a specific value.
    :param fields_name: A list with specific keys of the dictionary (see update_json new_parameters for details)
    :param deepness: Current level of nested dictionary
    :param dictionary:
    :param updated_value:
    :return:
    """
    for key, value in dictionary.items():
        if key == fields_name[deepness]:
            if deepness == len(fields_name) - 1 and type(updated_value) == dict:
                dictionary.update({fields_name[deepness]: updated_value})
            elif isinstance(value, dict):
                deepness += 1
                return find_parameter_and_update(fields_name, deepness, value, updated_value)
            else:
                dictionary.update({fields_name[deepness]: updated_value})


def folder_name_for_current_batch(json_folder_name, covid_model, output_path, no_options=None):
    """

    :param no_options: Remove index during folder creation
    :param json_folder_name:
    :param covid_model:
    :param output_path:
    :param no_options: Adds date and simulation index. Used for testing or when all simulations have the same name
    :return: a folder name for the current batch of simulations
    """
    if no_options is not None:
        if json_folder_name == "-":
            current_folder_name = "{}".format(covid_model)
        else:
            current_folder_name = "{}".format(json_folder_name)
    else:
        # Adds date to the folder name
        if json_folder_name == "-":
            current_folder_name = "{}_{}".format(covid_model, GlobalVar.Instance().current_date)
        else:
            current_folder_name = "{}_{}".format(json_folder_name, GlobalVar.Instance().current_date)
        simulation_index = get_simulation_index(current_folder_name, output_path, json_folder_name)
    if no_options is not None:
        folder_name = current_folder_name
    else:
        folder_name = current_folder_name + '_' + str(simulation_index)

    if not os.path.exists('{}{}'.format(output_path, folder_name)):
        os.makedirs(output_path + folder_name)
    return folder_name


def get_simulation_index(current_folder_name, output_path, json_folder_name):
    """
    Returns the current simulation index
    :param current_folder_name:
    :param output_path:
    :param json_folder_name:
    :return:
    """
    simulation_index = 1
    updated_output_path = output_path
    updated_current_folder_name = current_folder_name
    if '/' in json_folder_name:
        updated_output_path += json_folder_name.rsplit('/', 1)[0] + '/'
        updated_current_folder_name = current_folder_name.rsplit('/', 1)[1]

    if os.path.exists(updated_output_path):
        folders_name = os.listdir(updated_output_path)
        for folder in folders_name:
            if updated_current_folder_name in folder:
                simulation_index += 1
    else:
        os.makedirs(updated_output_path)
    return simulation_index
