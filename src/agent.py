#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By : Simon Pageaud
# License    : BSD-3-Clause Copyright 2021, CovDyn Developers.
# =============================================================================
"""
This module contains the agent class with a live method that describes
its behavior every time its called by the simulator.
"""
# =============================================================================
# Imports
# =============================================================================
import random as rnd
from global_variables import GlobalVar
from collections import deque


class Agent:

    def __init__(self, id_, compartment, population, age_id=0, time_step=1):
        self.id = id_
        self.compartment = compartment
        self.env = None
        self.action = None
        self.time_step = time_step

        # - Allows agents other than susceptible to act on iteration 0
        # - Forbids agents to act the iteration when they change state
        self.act_now = True
        self.just_infected = False

        self.age_id = age_id
        self.attributes = {'age': age_id, 'id': id_, 'alive': False, 'infectiousness': 0, 'variant': None}
        self.model_name = GlobalVar.Instance().COVID_model
        self.variant_name = None

        # Tracing
        self.tracing = GlobalVar.Instance().tracing
        if self.tracing:
            self.contacts_day = -1
            self.contacts_day_app = -1
            self.contacts_app = None
            self.got_app = False
            self.contacts = deque(maxlen=population.parameters['tracing_parameters'][
                'days_memory_for_contacts'])  # Stores contacts per day
            # functions
            self.contacts_saving = self.memory_update
            if population.parameters['tracing_parameters']['type']['application']['available']:
                if rnd.random() < population.parameters['tracing_parameters']['type']['application']['coverage']:
                    # Initialize a deque for contacts stored with the application
                    self.contacts_app = deque(
                        maxlen=population.parameters['tracing_parameters']['days_memory_for_contacts'])
                    self.got_app = True
                    self.contacts_saving = self.app_update

        # Vaccine
        self.vaccine = GlobalVar.Instance().vaccination

    def reset_for_calibration(self, values):
        self.attributes['alive'] = False
        GlobalVar.Instance().COVID_model_object.update_compartment(self,
                                                                   values['compartment_name'],
                                                                   values['stay_in_compartment'])
        self.env = None
        self.action = None
        self.variant_name = values['variant']

    def add_environment(self, env):
        """
        Adds the environment to the agent.
        :param env:
        """
        self.env = env

    def live(self):
        """
        Main function of the agent, called every iteration of the simulation.
        The agent acts only if its 'alive' (determined by its current state).
        """
        while self.attributes['alive']:
            GlobalVar.Instance().alive_agents_iter += 1
            if self.act_now:
                self.contacts_saving()
                self.normal_behavior()
                self.just_infected = False
            else:
                self.act_now = True
            yield self.env.timeout(self.time_step)

    def app_update(self):
        """
        Adds a new iteration in the deque of contacts stored on app.
        """
        if self.contacts_day_app != self.env.now:
            self.contacts_app.append([])
        self.memory_update()

    def memory_update(self):
        """
        Adds a new iteration in the deque of contacts stored.
        """
        if self.compartment.name != 'exposed':
            if self.contacts_day != self.env.now:
                # Check if the deque is already updated for this iteration
                self.contacts.append([])  # Adds a new day to the contacts deque
                self.contacts_day = self.env.now  # Stores current day

    def normal_behavior(self):
        """
        Compartment model step:
        - store the current compartment
        - compute the infection probability based on their compartment, beta values, etc...
        - infect
        """
        infection_probability = self.compartment.run_all(self.attributes)
        if infection_probability is not None and infection_probability > 0:
            self.tracing_infection(infection_probability)

    def tracing_infection(self, infection_probability):
        """
        Gathers contacts of the agent for current iteration and sorts them into infectious and non infectious
        contacts. For every contact, update the contact network of the simulation.
        Updates the compartment of every infectious contact.
        :param infection_probability: determined by the current compartment of the agent
        """
        newly_infected_agents, contacts_without_infection = GlobalVar.Instance().COVID_model_object.daily_contacts(
            self,
            infection_probability
        )
        self.contamination_of(newly_infected_agents)

    def contamination_of(self, newly_infected_agents):
        """
        Updates the memory of contacts of both current agent and contacted agents.
        For every contacted agent: changes its state to exposed, adds it to the environment.
        Increases the number of exposed agents and decreases the number of susceptible ones.
        :param newly_infected_agents: a list of exposed agents in this iteration.
        """
        GlobalVar.Instance().average_daily_contacts_infected.append(self.attributes['contacts'])
        for newly_infected_agent in newly_infected_agents:
            newly_infected_agent.act_now = False
            GlobalVar.Instance().COVID_model_object.fetch_variant_parameters(newly_infected_agent,
                                                                             GlobalVar.Instance().variants[
                                                                                 self.variant_name],
                                                                             newly_infected_agent.age_id)
            newly_infected_agent.compartment.next_state(newly_infected_agent.attributes)
            newly_infected_agent.action = newly_infected_agent.env.process(newly_infected_agent.live())
