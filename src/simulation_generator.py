#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By : Simon Pageaud
# License    : BSD-3-Clause Copyright 2021, CovDyn Developers.
# =============================================================================
"""
This module contains is used to launch a simulation.
"""
# =============================================================================
# Imports
# =============================================================================
import os
import simpy
import time
import argparse
from inspect import getmembers, isfunction

import parameters_simulation
import file_manipulation
from global_variables import GlobalVar
from population import Population


def launch_one_simulation_with(parameters):
    env = simpy.Environment()

    population = Population(parameters)
    population.add_environment(env)
    start_timer = time.perf_counter()
    env.run(until=parameters['simulation_duration'] / parameters['time_step'])
    population.wrap_up()
    end_timer = time.perf_counter()
    filename = population.filename.split('.txt')[0]
    extension = '.csv'
    for file_name, file in population.files.items():
        file_manipulation.write_to_csv(
            output_path + population.folder_name + '/' + filename + '_' + file_name + extension,
            file.columns, file.lines)

    print("Simulation complete in {:0.4f} seconds.".format(end_timer - start_timer))


def launch_simulation(new_parameters={}, with_options=None, simulation_loop=10,
                      json_file='../input/parameters.json', output='../results/'):
    """
    Loads simulation attributes, update its if necessary and then creates a new folder to store the updated
    json file and the output of the simulation.
    :param new_parameters: dictionary of simulation attributes
    :param with_options: Adds an index to folder name during its creation
    :param simulation_loop: number of simulations
    :param json_file:
    :param output:
    """
    # Load the specified json file from '../input/' (input_path + json_file_name)
    json_file = file_manipulation.load_json_file(json_file)

    # Update fields from the current json (See file_manipulation.update_json for details on dictionary structure.)
    file_manipulation.update_json(json_file, new_parameters)

    json_folder_name = json_file['simulation']['folder_name']
    covid_model = json_file['simulation']['COVID_model']

    # Create a new folder based on the saved folder_name in the json
    GlobalVar.Instance().folder_name = file_manipulation.folder_name_for_current_batch(json_folder_name, covid_model,
                                                                                       output_path, with_options)

    # Save it to the newly created folder
    file_manipulation.save_json_file(output_path + GlobalVar.Instance().folder_name + '/parameters.json', json_file)

    # Load the updated json and parse it
    parameters = file_manipulation.parse_parameters_file(
        output_path + GlobalVar.Instance().folder_name + '/parameters.json')

    # Start simulation loop
    for i in range(simulation_loop):
        print("Simulation {} out of {}.".format(i + 1, parameters['loop_number']))
        launch_one_simulation_with(parameters)
    return 0


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--function", help="function name from parameters_simulation.py",
                        choices=[i for (i, j) in getmembers(parameters_simulation, isfunction)])
    args = parser.parse_args()
    parameters_path = '../input'
    parameters_file = 'parameters.json'
    output_path = '../results/'

    # Gets the simulation scenario
    if args.function is None:
        # Default scenario is the Historical strain without vaccination.
        loop_dicts = parameters_simulation.historical_strain_without_vaccination()
    else:
        loop_dicts = getattr(parameters_simulation, args.function)()

    # For each set of parameters in loop_dicts, run a number of simulation specified in parameters.json
    for loop_dict in loop_dicts:
        print("Starts simulation {}".format(loop_dict['simulation:folder_name']))
        launch_simulation(loop_dict,
                          simulation_loop=loop_dict['simulation:loop_number'],
                          json_file=os.path.join(parameters_path, parameters_file),
                          output=output_path)
