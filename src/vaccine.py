#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By : Simon Pageaud
# License    : BSD-3-Clause Copyright 2021, CovDyn Developers.
# =============================================================================
"""
This module contains the vaccine class that stores its attributes
and manages injections.
"""
# =============================================================================
# Imports
# =============================================================================
import random as rnd


class Vaccine:

    def __init__(self, name, vac_prm, time_step):
        self.name = name
        self.attributes = {}
        self.attributes_per_injection_per_variant = {}
        for attribute_name, value in vac_prm.items():
            if type(value) == list:
                pass
            elif type(value) == dict:
                self.attributes_per_injection_per_variant[attribute_name] = self.sort_dict_values(value)
            else:
                self.attributes[attribute_name] = value
        self.attributes['iteration_before_next_injection'] /= time_step
        self.attributes['iteration_before_effects'] /= time_step

        # String values
        self.first_shot_active = 'first_shot_active'
        self.second_shot_active = 'second_shot_active'

    @staticmethod
    def sort_dict_values(current_dict):
        """
        Parse dict values from the json into attributes per variant
        :param current_dict:
        :return: a dict with variants as key and a dict of attributes as value
        """
        attributes_per_variant = {}
        for attribute_name, attribute_value in current_dict.items():  # attribute_value are always dicts here
            for variant_name, variant_value in attribute_value.items():  # variant_value is either list or float value
                if variant_name not in attributes_per_variant:
                    attributes_per_variant[variant_name] = {attribute_name: variant_value}
                else:
                    attributes_per_variant[variant_name][attribute_name] = variant_value
        return attributes_per_variant

    def get_first_shot(self, agent, GlobalVar, current_iteration):
        """
        Injects a vaccine first shot to the given agent. Based on the success rate, vaccination can be effective.
        Updates agent attributes of vaccine received and first shot
        :param agent: Agent object
        :param GlobalVar:
        :param current_iteration:
        """
        agent.attributes['vaccine_received'] = self.name
        agent.attributes['first_shot_ok'] = True
        if rnd.random() < self.attributes['success_rate']:
            agent.attributes['success_vaccinated'] = True
            GlobalVar.Instance().success_agents_vaccinated_per_iteration[
                current_iteration + int(self.attributes['iteration_before_effects'])].append(agent)
        GlobalVar.Instance().first_shot_agents_per_age[agent.age_id] += 1

    @staticmethod
    def get_second_shot(agent, GlobalVar):
        """
        If the vaccine requires a second shot, updates agent attributes.
        TODO some aspects of the second shot are not considered, for example:
        - if the first shot is not effective, could the second shot update that?
        - change attributes of the agent after the second shot
        :param agent: Agent object
        :param GlobalVar:
        """
        agent.attributes['second_shot_ok'] = True
        GlobalVar.Instance().second_shot_agents_per_age[agent.age_id] += 1

    def update_attributes_for_injection(self, agent, current_injection):
        """
        Updates attributes after the specified shot based on the agent variant.
        :param agent:
        :param current_injection: name of the json dict (either "attributes_first_injection" or
        "attributes_second_injection"
        """
        if current_injection == "attributes_first_injection":
            agent.attributes[self.first_shot_active] = True
        elif current_injection == "attributes_second_injection":
            agent.attributes[self.second_shot_active] = True
        agent_variant = agent.variant_name if agent.variant_name is not None else "regular"
        if current_injection in self.attributes_per_injection_per_variant and agent_variant in \
                self.attributes_per_injection_per_variant[current_injection]:
            for attribute, values in self.attributes_per_injection_per_variant[current_injection][agent_variant].items():
                if type(values) == list:  # Age based attribute
                    agent.attributes[attribute] = values[agent.age_id]
                else:
                    agent.attributes[attribute] = values

    def update_attributes(self, agent):
        """
        Update agent attributes when fetching parameters of the variant.
        :param agent:
        """
        if self.second_shot_active in agent.attributes and agent.attributes[self.second_shot_active]:
            self.update_attributes_for_injection(agent, 'attributes_second_injection')
        elif self.first_shot_active in agent.attributes and agent.attributes[self.first_shot_active]:
            self.update_attributes_for_injection(agent, 'attributes_first_injection')
