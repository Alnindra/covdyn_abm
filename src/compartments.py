#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By : Simon Pageaud
# License    : BSD-3-Clause Copyright 2021, CovDyn Developers.
# =============================================================================
"""
This module contains the compartments of the CovDyn Model and how
the transition between compartments are made.
"""
# =============================================================================
# Imports
# =============================================================================
from global_variables import GlobalVar

import random as rnd
import math
from scipy.special import gamma


class StateMachine:
    """
    Generic description of the Finite State Machine that is a compartment.
    """
    def __init__(self, initial_state):
        self.current_state = initial_state

    def run_all(self, attributes):
        value = self.current_state.run(attributes)
        self.current_state = self.current_state.next(attributes)
        return value

    def next_state(self, attributes=None):
        self.current_state = self.current_state.next_state(attributes)

    def final_state(self, attributes=None):
        self.current_state = self.current_state.final_state(attributes)


class State:
    """
    Basic state class with error message when all the methods are not implemented.
    """
    def __init__(self, name, short_name, attributes=None):
        self.name = name
        self.short_name = short_name
        self.time_step = GlobalVar.Instance().time_step
        self.time = 0

    def run(self, attributes):
        assert 0, "run not implemented"

    def next(self, attributes):
        assert 0, "next not implemented"

    def next_state(self, attributes=None):
        assert 0, "next_state not implemented"

    def final_state(self, attributes=None):
        print(self.name)
        assert 0, "final_state not implemented"

    def geometric_distribution(self, attributes):
        """
        Default distribution following an exponential
        :param attributes:
        :return:
        """
        assert 0, "final_state not implemented"

    def weibull_distribution(self, attributes):
        assert 0, "final_state not implemented"

    def transition_distribution_function(self, attributes):
        """
        Specifies the transition distribution function for the simulation.
        :return:
        """
        if GlobalVar.Instance().transition_type == "Weibull":
            return self.weibull_distribution(attributes)
        return self.geometric_distribution(attributes)

    def __str__(self):
        return self.name


class Susceptible(State):

    def __init__(self, attributes=None):
        super().__init__("susceptible", "S", attributes)

    def run(self, attributes):
        pass

    def next(self, attributes):
        return self

    def next_state(self, attributes=None):
        compartment_change(self.name, "exposed", attributes)
        attributes['alive'] = True
        return Exposed(attributes)

    def final_state(self, attributes=None):
        compartment_change(self.name, "removed", attributes)
        return Removed(attributes)


class Exposed(State):

    def __init__(self, attributes=None):
        super().__init__("exposed", "E", attributes)
        attributes['alive'] = True
        self.time_step = GlobalVar.Instance().time_step

    def run(self, attributes):
        self.time += self.time_step

    def next(self, attributes):
        if rnd.random() <= self.transition_distribution_function(attributes):
            compartment_change(self.name, "prodromic", attributes)
            return ProdromicPhase(attributes)
        return self

    def final_state(self, attributes=None):
        compartment_change(self.name, "removed", attributes)
        attributes['alive'] = False
        return Removed(attributes)

    def geometric_distribution(self, attributes):
        return self.time_step/(attributes['t_i'] - attributes['t_p'])

    def weibull_distribution(self, attributes):
        lambda_ = (attributes['t_i'] - attributes['t_p'])/gamma(1 + 1/attributes['weibull_k'])
        return 1 - math.exp(math.pow((self.time - 0.5 * self.time_step)/lambda_, attributes['weibull_k']) -
                            math.pow((self.time + 0.5 * self.time_step)/lambda_, attributes['weibull_k']))


class ProdromicPhase(State):

    def __init__(self, attributes=None):
        super().__init__("prodromic", "Ip", attributes)
        attributes['alive'] = True
        proba_next_state = rnd.random()
        p_Iss = (1-attributes['p_A'])*attributes['p_H']
        p_Ims = (1-attributes['p_A'])*attributes['p_Ims']
        p_Ips = (1-attributes['p_A'])*attributes['p_Ips']
        if proba_next_state < p_Iss:
            self.next_state = "severe_symptoms"
        elif proba_next_state < p_Iss + p_Ims:
            self.next_state = "medium_symptoms"
        elif proba_next_state < p_Iss + p_Ips + p_Ims:
            self.next_state = "paucisymptomatic"
        else:
            self.next_state = "asymptomatic"
        self.time_step = GlobalVar.Instance().time_step

    def run(self, attributes):
        self.time += self.time_step
        return infection_probability(attributes, attributes['i_Ip'], self.time_step)

    def next(self, attributes):
        if rnd.random() < self.transition_distribution_function(attributes):
            if self.next_state == "severe_symptoms":
                compartment_change(self.name, "severe_symptoms", attributes)
                return SevereSymptoms(attributes)
            elif self.next_state == "medium_symptoms":
                compartment_change(self.name, "medium_symptoms", attributes)
                return MediumSymptoms(attributes)
            elif self.next_state == "paucisymptomatic":
                compartment_change(self.name, "paucisymptomatic", attributes)
                return Paucisymptomatic(attributes)
            else:
                compartment_change(self.name, "asymptomatic", attributes)
                return Asymptomatic(attributes)
        return self

    def final_state(self, attributes=None):
        compartment_change(self.name, "removed", attributes)
        attributes['alive'] = False
        return Removed(attributes)

    def geometric_distribution(self, attributes):
        return self.time_step/attributes['t_p']

    def weibull_distribution(self, attributes):
        return 1 - math.exp(math.pow((self.time - 0.5 * self.time_step)/attributes['r_t_p'], attributes['weibull_k']) -
                            math.pow((self.time + 0.5 * self.time_step)/attributes['r_t_p'], attributes['weibull_k']))


class Asymptomatic(State):

    def __init__(self, attributes=None):
        super().__init__("asymptomatic", "A", attributes)
        attributes['alive'] = True
        self.time_step = GlobalVar.Instance().time_step

    def run(self, attributes):
        self.time += self.time_step
        return infection_probability(attributes, attributes['i_A'], self.time_step)

    def next(self, attributes):
        if rnd.random() <= self.transition_distribution_function(attributes):
            compartment_change(self.name, "removed", attributes)
            attributes['alive'] = False
            return Removed(attributes)
        return self

    def final_state(self, attributes=None):
        compartment_change(self.name, "removed", attributes)
        attributes['alive'] = False
        return Removed(attributes)

    def geometric_distribution(self, attributes):
        return self.time_step/attributes['t_s']

    def weibull_distribution(self, attributes):
        return 1 - math.exp(math.pow((self.time - 0.5 * self.time_step)/attributes['r_t_s'], attributes['weibull_k']) -
                            math.pow((self.time + 0.5 * self.time_step)/attributes['r_t_s'], attributes['weibull_k']))


class Paucisymptomatic(State):

    def __init__(self, attributes=None):
        super().__init__("paucisymptomatic", "Ips", attributes)
        attributes['alive'] = True
        self.time_step = GlobalVar.Instance().time_step

    def run(self, attributes):
        self.time += self.time_step
        return infection_probability(attributes, attributes['i_Ips'], self.time_step)

    def next(self, attributes):
        if rnd.random() <= self.transition_distribution_function(attributes):
            compartment_change(self.name, "removed", attributes)
            attributes['alive'] = False
            return Removed(attributes)
        return self

    def final_state(self, attributes=None):
        compartment_change(self.name, "removed", attributes)
        attributes['alive'] = False
        return Removed(attributes)

    def geometric_distribution(self, attributes):
        return self.time_step/attributes['t_s']

    def weibull_distribution(self, attributes):
        return 1 - math.exp(math.pow((self.time - 0.5 * self.time_step)/attributes['r_t_s'], attributes['weibull_k']) -
                            math.pow((self.time + 0.5 * self.time_step)/attributes['r_t_s'], attributes['weibull_k']))


class MediumSymptoms(State):

    def __init__(self, attributes=None):
        super().__init__("medium_symptoms", "Ims", attributes)
        attributes['alive'] = True
        self.time_step = GlobalVar.Instance().time_step

    def run(self, attributes):
        self.time += self.time_step
        return infection_probability(attributes, attributes['i_Ims'], self.time_step)

    def next(self, attributes):
        if rnd.random() <= self.transition_distribution_function(attributes):
            compartment_change(self.name, "removed", attributes)
            attributes['alive'] = False
            return Removed(attributes)
        return self

    def final_state(self, attributes=None):
        compartment_change(self.name, "removed", attributes)
        attributes['alive'] = False
        return Removed(attributes)

    def geometric_distribution(self, attributes):
        return self.time_step/attributes['t_s']

    def weibull_distribution(self, attributes):
        return 1 - math.exp(math.pow((self.time - 0.5 * self.time_step)/attributes['r_t_s'], attributes['weibull_k']) -
                            math.pow((self.time + 0.5 * self.time_step)/attributes['r_t_s'], attributes['weibull_k']))


class SevereSymptoms(State):

    def __init__(self, attributes=None):
        super().__init__("severe_symptoms", "Iss", attributes)
        attributes['alive'] = True
        self.time_step = GlobalVar.Instance().time_step

    def run(self, attributes):
        self.time += self.time_step
        return infection_probability(attributes, attributes['i_Iss'], self.time_step)

    def next(self, attributes):
        if rnd.random() <= self.transition_distribution_function(attributes):
            compartment_change(self.name, "hospitalized", attributes)
            GlobalVar.Instance().cumulated_agents_age_variant['hospitalized'][attributes['variant']][attributes['age']] += 1
            return Hospitalized(attributes)
        return self

    def final_state(self, attributes=None):
        compartment_change(self.name, "removed", attributes)
        attributes['alive'] = False
        return Removed(attributes)

    def geometric_distribution(self, attributes):
        return self.time_step/attributes['t_bh']

    def weibull_distribution(self, attributes):
        return 1 - math.exp(math.pow((self.time - 0.5 * self.time_step)/attributes['r_t_bh'], attributes['weibull_k']) -
                            math.pow((self.time + 0.5 * self.time_step)/attributes['r_t_bh'], attributes['weibull_k']))


class Hospitalized(State):

    def __init__(self, attributes=None):
        super().__init__("hospitalized", "H1", attributes)
        attributes['alive'] = True
        self.time_step = GlobalVar.Instance().time_step

    def run(self, attributes):
        self.time += self.time_step

    def next(self, attributes):
        proba_next_state = rnd.random()
        if proba_next_state <= attributes['p_ICU|H1']:  # Change to ICU
            compartment_change(self.name, "ICU", attributes)
            GlobalVar.Instance().cumulated_agents_age_variant['ICU'][attributes['variant']][attributes['age']] += 1
            return ICU(attributes)
        elif proba_next_state <= attributes['p_ICU|H1'] + attributes['p_H2|H1']:  # Change to HospitalizedSecond (H2)
            compartment_change(self.name, "hospitalizedSecond", attributes)
            return HospitalizedSecond(attributes)
        return self

    def geometric_distribution(self, attributes):
        return self.time_step/attributes['t_H']

    def weibull_distribution(self, attributes):
        return 1 - math.exp(math.pow((self.time - 0.5 * self.time_step)/attributes['r_t_H1'], attributes['weibull_k']) -
                            math.pow((self.time + 0.5 * self.time_step)/attributes['r_t_H1'], attributes['weibull_k']))


class HospitalizedSecond(State):

    def __init__(self, attributes=None):
        super().__init__("hospitalizedSecond", "H2", attributes)
        attributes['alive'] = True
        self.time_step = GlobalVar.Instance().time_step

    def run(self, attributes):
        self.time += self.time_step

    def next(self, attributes):
        iteration_probability = rnd.random()
        if iteration_probability <= attributes['p_D|H2']:  # Change to deceased
            compartment_change(self.name, "deceased", attributes)
            remove_agent(attributes)
            return Deceased(attributes)
        elif iteration_probability < attributes['p_D|H2'] + attributes['p_R|H2']:  # Change to Removed
            compartment_change(self.name, "removed", attributes)
            return Removed(attributes)
        return self

    def geometric_distribution(self, attributes):
        return self.time_step/attributes['t_H']

    def weibull_distribution(self, attributes):
        return 1 - math.exp(math.pow((self.time - 0.5 * self.time_step)/attributes['r_t_H2'], attributes['weibull_k']) -
                            math.pow((self.time + 0.5 * self.time_step)/attributes['r_t_H2'], attributes['weibull_k']))


class HospitalizedThird(State):

    def __init__(self, attributes=None):
        super().__init__("hospitalizedThird", "H3", attributes)
        attributes['alive'] = True
        self.time_step = GlobalVar.Instance().time_step

    def run(self, attributes):
        self.time += self.time_step

    def next(self, attributes):
        iteration_probability = rnd.random()
        if iteration_probability <= attributes['p_D|H3']:  # Change to Deceased
            compartment_change(self.name, "deceased", attributes)
            remove_agent(attributes)
            return Deceased(attributes)
        elif iteration_probability <= attributes['p_D|H3'] + attributes['p_R|H3']:  # Change to Removed
            compartment_change(self.name, "removed", attributes)
            return Removed(attributes)
        return self

    def geometric_distribution(self, attributes):
        return self.time_step/attributes['t_H']

    def weibull_distribution(self, attributes):
        return 1 - math.exp(math.pow((self.time - 0.5 * self.time_step)/attributes['r_t_H3'], attributes['weibull_k']) -
                            math.pow((self.time + 0.5 * self.time_step)/attributes['r_t_H3'], attributes['weibull_k']))


class ICU(State):

    def __init__(self, attributes=None):
        super().__init__("ICU", "ICU", attributes)
        attributes['alive'] = True
        self.time_step = GlobalVar.Instance().time_step

    def run(self, attributes):
        self.time += self.time_step

    def next(self, attributes):
        if rnd.random() <= attributes['p_H3|ICU']:
            compartment_change(self.name, "hospitalizedThird", attributes)
            return HospitalizedThird(attributes)
        return self

    def geometric_distribution(self, attributes):
        return self.time_step/attributes['t_ICU']

    def weibull_distribution(self, attributes):
        return 1 - math.exp(math.pow((self.time - 0.5 * self.time_step)/attributes['r_t_ICU'], attributes['weibull_k']) -
                            math.pow((self.time + 0.5 * self.time_step)/attributes['r_t_ICU'], attributes['weibull_k']))


class Removed(State):

    def __init__(self, attributes=None):
        super().__init__("removed", "R", attributes)
        attributes['alive'] = False

    def run(self, attributes):
        pass

    def next(self, attributes):
        return self

    def final_state(self, attributes=None):
        return self


class Deceased(State):

    def __init__(self, attributes=None):
        super().__init__("deceased", "D", attributes)
        attributes['alive'] = False

    def run(self, attributes):
        pass

    def next(self, attributes):
        return self


def remove_agent(attributes):
    for agent in GlobalVar.Instance().population_of_agents_per_age[attributes['age']]:
        if agent.id == attributes['id']:
            GlobalVar.Instance().population_of_agents_per_age[attributes['age']].remove(agent)
            break


class Compartment(StateMachine):
    def __init__(self, initial_state=None, attributes=None):
        if initial_state is None:
            StateMachine.__init__(self, Susceptible())
        elif initial_state == 'susceptible':
            StateMachine.__init__(self, Susceptible())
        elif initial_state == 'exposed':
            StateMachine.__init__(self, Exposed(attributes))
        elif initial_state == 'prodromic':
            StateMachine.__init__(self, ProdromicPhase(attributes))
        elif initial_state == 'asymptomatic':
            StateMachine.__init__(self, Asymptomatic(attributes))
        elif initial_state == 'paucisymptomatic':
            StateMachine.__init__(self, Paucisymptomatic(attributes))
        elif initial_state == 'medium_symptoms':
            StateMachine.__init__(self, MediumSymptoms(attributes))
        elif initial_state == 'severe_symptoms':
            StateMachine.__init__(self, SevereSymptoms(attributes))
        elif initial_state == 'hospitalized':
            StateMachine.__init__(self, Hospitalized(attributes))
        elif initial_state == 'hospitalizedSecond':
            StateMachine.__init__(self, HospitalizedSecond(attributes))
        elif initial_state == 'hospitalizedThird':
            StateMachine.__init__(self, HospitalizedThird(attributes))
        elif initial_state == 'ICU':
            StateMachine.__init__(self, ICU(attributes))
        elif initial_state == 'removed':
            StateMachine.__init__(self, Removed(attributes))
        elif initial_state == 'deceased':
            StateMachine.__init__(self, Deceased())

    @property
    def name(self):
        return self.current_state.name

    @property
    def short_name(self):
        return self.current_state.short_name


def compartment_change(current_compartment, next_compartment, attributes):
    GlobalVar.Instance().number_agents_per_compartments[current_compartment] -= 1
    GlobalVar.Instance().number_agents_per_compartments[next_compartment] += 1
    GlobalVar.Instance().number_agents_per_compartments_per_age[current_compartment][attributes['age']] -= 1
    GlobalVar.Instance().number_agents_per_compartments_per_age[next_compartment][attributes['age']] += 1

    # Adds variant per compartment and per age
    if attributes['variant'] is None:
        variant_name = 'regular'
    else:
        variant_name = attributes['variant']
    if current_compartment != "susceptible":
        GlobalVar.Instance().number_agents_per_compartments_per_age_per_variant[current_compartment][variant_name][
            attributes['age']] -= 1
        GlobalVar.Instance().number_agents_per_compartments_per_age_per_variant[next_compartment][variant_name][
            attributes['age']] += 1
    else:
        GlobalVar.Instance().number_agents_per_compartments_per_age_per_variant[next_compartment][variant_name][
            attributes['age']] += 1
    GlobalVar.Instance().agents_in_per_comp_age_variant_iter[next_compartment][variant_name][attributes['age']] += 1
    GlobalVar.Instance().agents_out_per_comp_age_variant_iter[current_compartment][variant_name][attributes['age']] += 1


def infection_probability(attributes, infectiousness, time_step=1):
    beta2 = GlobalVar.Instance().beta2[GlobalVar.Instance().env.now][attributes['age']]
    return attributes['beta1'] * time_step * beta2 * infectiousness
