#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By : Simon Pageaud
# =============================================================================
"""
This module contains the three available scenarios and parameters used for
their simulation.
"""
# =============================================================================
# Imports
# =============================================================================
import inspect


def historical_strain_without_vaccination():
    """
    Experimentation for:
    - no vaccine
    - no variant
    - three scenarios: vectors of beta2 multiplied by a extended coefficient; intensive coefficient; and switch between
    its regular value and intensive coefficient)
    :return: an array of simulation parameters
    """
    print('Start a simulation with {}'.format(inspect.currentframe().f_code.co_name))
    loop_dicts = []
    beta1 = 0.043
    beta2 = {'extended': [0.14263832148900557,
                          0.14263832148900557,
                          0.14263832148900557,
                          0.14263832148900557,
                          0.21684912145983778,
                          0.21684912145983778,
                          0.09512233780076533,
                          0.09512233780076533,
                          0.043417796014587506,
                          0.1306010764544347,
                          0.12247917509746761,
                          0.18306554283556445,
                          0.19463630519256617,
                          0.19463630519256617,
                          1.9907690993899558,
                          1.9907690993899558,
                          2.0393399666307706],
             'intensive': [0.2089935922949808,
                           0.2089935922949808,
                           0.2089935922949808,
                           0.2089935922949808,
                           0.3177272166890673,
                           0.3177272166890673,
                           0.13937319842908358,
                           0.13937319842908358,
                           0.06361573148011815,
                           0.19135662731356243,
                           0.179456421792784,
                           0.2682275353722829,
                           0.28518101018422337,
                           0.28518101018422337,
                           2.9168738188174763,
                           2.9168738188174763,
                           2.9880398274999527],
             'relaxed': [0.26688357961109804,
                         0.26688357961109804,
                         0.26688357961109804,
                         0.26688357961109804,
                         0.4057357739952382,
                         0.4057357739952382,
                         0.17797874896614715,
                         0.17797874896614715,
                         0.08123691234049424,
                         0.2443612797834896,
                         0.22916478781157104,
                         0.34252497411202437,
                         0.3641744610411312,
                         0.3641744610411312,
                         3.7248305916534896,
                         3.7248305916534896,
                         3.815709163265419]}
    r_effective = {'extended': 0.525, 'intensive': 0.773, 'relaxed': 1}
    scenarios = {
        "1wave_intensive": {
            "0": {
                "T_b": 0,
                "T_e": 59,
                "beta2_t1": beta2['relaxed'],
                "R_0": r_effective['relaxed']
            },
            "1": {
                "T_b": 59,
                "T_e": 92,
                "beta2_t1": beta2['extended'],
                "R_0": r_effective['extended']
            },
            "2": {
                "T_b": 92,
                "T_e": 122,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            },
            "3": {
                "T_b": 122,
                "T_e": 675,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            }
        },
        "1wave_extended": {
            "0": {
                "T_b": 0,
                "T_e": 59,
                "beta2_t1": beta2['relaxed'],
                "R_0": r_effective['relaxed']
            },
            "1": {
                "T_b": 59,
                "T_e": 92,
                "beta2_t1": beta2['extended'],
                "R_0": r_effective['extended']
            },
            "2": {
                "T_b": 92,
                "T_e": 122,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            },
            "3": {
                "T_b": 122,
                "T_e": 675,
                "beta2_t1": beta2['extended'],
                "R_0": r_effective['extended']
            }
        },
        "relaxation": {
            "0": {
                "T_b": 0,
                "T_e": 59,
                "beta2_t1": beta2['relaxed'],
                "R_0": r_effective['relaxed']
            },
            "1": {
                "T_b": 59,
                "T_e": 92,
                "beta2_t1": beta2['extended'],
                "R_0": r_effective['extended']
            },
            "2": {
                "T_b": 92,
                "T_e": 122,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            },
            "3": {
                "T_b": 122,
                "T_e": 167,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            },
            "4": {
                "T_b": 167,
                "T_e": 211,
                "beta2_t1": beta2['relaxed'],
                "R_0": r_effective['relaxed']
            },
            "5": {
                "T_b": 211,
                "T_e": 256,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            },
            "6": {
                "T_b": 256,
                "T_e": 300,
                "beta2_t1": beta2['relaxed'],
                "R_0": r_effective['relaxed']
            },
            "7": {
                "T_b": 300,
                "T_e": 344,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            },
            "8": {
                "T_b": 344,
                "T_e": 389,
                "beta2_t1": beta2['relaxed'],
                "R_0": r_effective['relaxed']
            },
            "9": {
                "T_b": 389,
                "T_e": 434,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            },
            "10": {
                "T_b": 434,
                "T_e": 478,
                "beta2_t1": beta2['relaxed'],
                "R_0": r_effective['relaxed']
            },
            "11": {
                "T_b": 478,
                "T_e": 523,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            },
            "12": {
                "T_b": 523,
                "T_e": 567,
                "beta2_t1": beta2['relaxed'],
                "R_0": r_effective['relaxed']
            },
            "13": {
                "T_b": 567,
                "T_e": 612,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            },
            "14": {
                "T_b": 612,
                "T_e": 656,
                "beta2_t1": beta2['relaxed'],
                "R_0": r_effective['relaxed']
            },
            "15": {
                "T_b": 656,
                "T_e": 701,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            }
        }
    }
    for scenario_name, dict_scenario in scenarios.items():
        new_parameters = {
            "simulation:folder_name": "CovDyn_Model_no_variant_no_vaccine/{}".format(scenario_name),
            "simulation:simulation_duration": 675,
            "simulation:variant": ["regular"],
            "simulation:calibration": 0,
            "simulation:loop_number": 50,
            "vaccination:available": 0,
            "population:population_distribution:reduction_factor": 0.01,
            "COVID:initial_compartment_per_age:regular:details:percentage_removed": 0.08,
            "COVID:initial_compartment_per_age:regular:targets:prevalence:hospitalized": [
                1,
                1,
                1,
                2,
                12,
                13,
                13,
                13,
                12,
                13,
                13,
                13,
                23,
                23,
                19,
                20,
                73],
            "COVID:initial_compartment_per_age:regular:targets:prevalence:hospitalizedSecond": [
                6,
                6,
                4,
                4,
                14,
                13,
                21,
                21,
                48,
                48,
                104,
                104,
                188,
                188,
                248,
                247,
                775],
            "COVID:initial_compartment_per_age:regular:targets:prevalence:hospitalizedThird": [
                0,
                0,
                4,
                4,
                14,
                13,
                21,
                21,
                48,
                48,
                104,
                104,
                188,
                188,
                248,
                247,
                775],
            "COVID:initial_compartment_per_age:regular:targets:prevalence:ICU": [
                1,
                2,
                0,
                1,
                3,
                4,
                7,
                7,
                13,
                14,
                26,
                26,
                53,
                53,
                56,
                57,
                54],
            "COVID:initial_compartment_per_age:regular:targets:incidence:hospitalized": [
                1,
                1,
                1,
                2,
                12,
                13,
                13,
                13,
                12,
                13,
                13,
                13,
                23,
                23,
                19,
                20,
                73],
            "COVID:model:CovDyn_Model:attributes:p_A:regular": 0.2,
            "COVID:model:CovDyn_Model:attributes:beta1:regular": beta1,
            "COVID:model:CovDyn_Model:sanitary_update": dict_scenario
        }
        loop_dicts.append(new_parameters)
    return loop_dicts


def historical_variant_strains_without_vaccination():
    """
    Experimentation for:
    - no vaccine
    - variant and historical strain
    - three scenarios: vectors of beta2 multiplied by a extended coefficient; intensive coefficient; and switch between
    its regular value and intensive coefficient)
    :return: an array of simulation parameters
    """
    print('Start a simulation with {}'.format(inspect.currentframe().f_code.co_name))
    loop_dicts = []
    beta1 = 0.043
    beta2 = {'extended': [0.14263832148900557,
                          0.14263832148900557,
                          0.14263832148900557,
                          0.14263832148900557,
                          0.21684912145983778,
                          0.21684912145983778,
                          0.09512233780076533,
                          0.09512233780076533,
                          0.043417796014587506,
                          0.1306010764544347,
                          0.12247917509746761,
                          0.18306554283556445,
                          0.19463630519256617,
                          0.19463630519256617,
                          1.9907690993899558,
                          1.9907690993899558,
                          2.0393399666307706],
             'intensive': [0.2089935922949808,
                           0.2089935922949808,
                           0.2089935922949808,
                           0.2089935922949808,
                           0.3177272166890673,
                           0.3177272166890673,
                           0.13937319842908358,
                           0.13937319842908358,
                           0.06361573148011815,
                           0.19135662731356243,
                           0.179456421792784,
                           0.2682275353722829,
                           0.28518101018422337,
                           0.28518101018422337,
                           2.9168738188174763,
                           2.9168738188174763,
                           2.9880398274999527],
             'relaxed': [0.26688357961109804,
                         0.26688357961109804,
                         0.26688357961109804,
                         0.26688357961109804,
                         0.4057357739952382,
                         0.4057357739952382,
                         0.17797874896614715,
                         0.17797874896614715,
                         0.08123691234049424,
                         0.2443612797834896,
                         0.22916478781157104,
                         0.34252497411202437,
                         0.3641744610411312,
                         0.3641744610411312,
                         3.7248305916534896,
                         3.7248305916534896,
                         3.815709163265419]}
    r_effective = {'extended': 0.525, 'intensive': 0.773, 'relaxed': 1}
    iteration_shift_probability_right_side = [183]
    beta3 = 1.57212382
    scenarios = {
        "1wave_intensive": {
            "0": {
                "T_b": 0,
                "T_e": 59,
                "beta2_t1": beta2['relaxed'],
                "R_0": r_effective['relaxed']
            },
            "1": {
                "T_b": 59,
                "T_e": 92,
                "beta2_t1": beta2['extended'],
                "R_0": r_effective['extended']
            },
            "2": {
                "T_b": 92,
                "T_e": 122,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            },
            "3": {
                "T_b": 122,
                "T_e": 675,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            }
        },
        "1wave_extended": {
            "0": {
                "T_b": 0,
                "T_e": 59,
                "beta2_t1": beta2['relaxed'],
                "R_0": r_effective['relaxed']
            },
            "1": {
                "T_b": 59,
                "T_e": 92,
                "beta2_t1": beta2['extended'],
                "R_0": r_effective['extended']
            },
            "2": {
                "T_b": 92,
                "T_e": 122,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            },
            "3": {
                "T_b": 122,
                "T_e": 675,
                "beta2_t1": beta2['extended'],
                "R_0": r_effective['extended']
            }
        },
        "relaxation": {
            "0": {
                "T_b": 0,
                "T_e": 59,
                "beta2_t1": beta2['relaxed'],
                "R_0": r_effective['relaxed']
            },
            "1": {
                "T_b": 59,
                "T_e": 92,
                "beta2_t1": beta2['extended'],
                "R_0": r_effective['extended']
            },
            "2": {
                "T_b": 92,
                "T_e": 122,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            },
            "3": {
                "T_b": 122,
                "T_e": 167,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            },
            "4": {
                "T_b": 167,
                "T_e": 211,
                "beta2_t1": beta2['relaxed'],
                "R_0": r_effective['relaxed']
            },
            "5": {
                "T_b": 211,
                "T_e": 256,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            },
            "6": {
                "T_b": 256,
                "T_e": 300,
                "beta2_t1": beta2['relaxed'],
                "R_0": r_effective['relaxed']
            },
            "7": {
                "T_b": 300,
                "T_e": 344,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            },
            "8": {
                "T_b": 344,
                "T_e": 389,
                "beta2_t1": beta2['relaxed'],
                "R_0": r_effective['relaxed']
            },
            "9": {
                "T_b": 389,
                "T_e": 434,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            },
            "10": {
                "T_b": 434,
                "T_e": 478,
                "beta2_t1": beta2['relaxed'],
                "R_0": r_effective['relaxed']
            },
            "11": {
                "T_b": 478,
                "T_e": 523,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            },
            "12": {
                "T_b": 523,
                "T_e": 567,
                "beta2_t1": beta2['relaxed'],
                "R_0": r_effective['relaxed']
            },
            "13": {
                "T_b": 567,
                "T_e": 612,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            },
            "14": {
                "T_b": 612,
                "T_e": 656,
                "beta2_t1": beta2['relaxed'],
                "R_0": r_effective['relaxed']
            },
            "15": {
                "T_b": 656,
                "T_e": 701,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            }
        }
    }
    for scenario_name, dict_scenario in scenarios.items():
        new_parameters = {
            "simulation:folder_name": "CovDyn_Model_variant_no_vaccine/{}".format(scenario_name),
            "simulation:simulation_duration": 675,
            "simulation:variant": ["regular", "B.1.1.7"],
            "simulation:calibration": 0,
            "simulation:loop_number": 50,
            "vaccination:available": 0,
            "population:population_distribution:reduction_factor": 0.01,
            "COVID:initial_compartment_per_age:regular:details:percentage_removed": 0.08,
            "COVID:initial_compartment_per_age:regular:targets:prevalence:hospitalized": [
                1,
                1,
                1,
                2,
                12,
                13,
                13,
                13,
                12,
                13,
                13,
                13,
                23,
                23,
                19,
                20,
                73],
            "COVID:initial_compartment_per_age:regular:targets:prevalence:hospitalizedSecond": [
                6,
                6,
                4,
                4,
                14,
                13,
                21,
                21,
                48,
                48,
                104,
                104,
                188,
                188,
                248,
                247,
                775],
            "COVID:initial_compartment_per_age:regular:targets:prevalence:hospitalizedThird": [
                0,
                0,
                4,
                4,
                14,
                13,
                21,
                21,
                48,
                48,
                104,
                104,
                188,
                188,
                248,
                247,
                775],
            "COVID:initial_compartment_per_age:regular:targets:prevalence:ICU": [
                1,
                2,
                0,
                1,
                3,
                4,
                7,
                7,
                13,
                14,
                26,
                26,
                53,
                53,
                56,
                57,
                54],
            "COVID:initial_compartment_per_age:regular:targets:incidence:hospitalized": [
                1,
                1,
                1,
                2,
                12,
                13,
                13,
                13,
                12,
                13,
                13,
                13,
                23,
                23,
                19,
                20,
                73],
            "COVID:initial_compartment_per_age:B.1.1.7:details:percentage_intercept": 0.033,
            "COVID:initial_compartment_per_age:B.1.1.7:details:start": 131,
            "COVID:model:CovDyn_Model:attributes:p_A:regular": 0.2,
            "COVID:model:CovDyn_Model:attributes:p_A:B.1.1.7": 0.2,
            "COVID:model:CovDyn_Model:attributes:beta1:regular": beta1,
            "COVID:model:CovDyn_Model:attributes:beta1:B.1.1.7": beta1,
            "COVID:model:CovDyn_Model:sanitary_update": dict_scenario,
            "COVID:model:CovDyn_Model:attributes:i_A:B.1.1.7": 0.55 * beta3,
            "COVID:model:CovDyn_Model:attributes:i_Ip:B.1.1.7": 0.55 * beta3,
            "COVID:model:CovDyn_Model:attributes:i_Ips:B.1.1.7": 0.55 * beta3,
            "COVID:model:CovDyn_Model:attributes:i_Ims:B.1.1.7": 1 * beta3,
            "COVID:model:CovDyn_Model:attributes:i_Iss:B.1.1.7": 1 * beta3,
            "COVID:attributes_update:iterations": iteration_shift_probability_right_side,
            "COVID:attributes_update:0:attributes:p_H2|H1:regular": [0.9479088828090996, 0.9479088828090996,
                                                                     0.966777512062082, 0.966777512062082,
                                                                     0.9018353444164374, 0.9018353444164374,
                                                                     0.9183248612317887, 0.9183248612317887,
                                                                     0.8730129124064093, 0.8730129124064093,
                                                                     0.8413026634950577, 0.8413026634950577,
                                                                     0.39582793927434223, 0.39582793927434223,
                                                                     0.7323245088965133, 0.7323245088965133,
                                                                     0.8946574656948076],
            "COVID:attributes_update:0:attributes:p_ICU|H1:regular": [0.05209111719090043, 0.05209111719090043,
                                                                      0.03073764532567787, 0.03073764532567787,
                                                                      0.076998949309656, 0.076998949309656,
                                                                      0.07052315993853264, 0.07052315993853264,
                                                                      0.11574303671502471, 0.11574303671502471,
                                                                      0.1571068344887177, 0.1571068344887177,
                                                                      0.2797592947742128, 0.2797592947742128,
                                                                      0.26767549110348665, 0.26767549110348665,
                                                                      0.09833221081844357],
            "COVID:attributes_update:0:attributes:p_H3|ICU:regular": [0.07928153703022643, 0.07928153703022643,
                                                                      0.03333333333333333, 0.03333333333333333,
                                                                      0.07006318123621075, 0.07006318123621075,
                                                                      0.03333333333333333, 0.03333333333333333,
                                                                      0.03333333333333333, 0.03333333333333333,
                                                                      0.03333333333333333, 0.03333333333333333,
                                                                      0.07534078822104602, 0.07534078822104602,
                                                                      0.05682056530413365, 0.05682056530413365,
                                                                      0.13932869597489814],
            "COVID:attributes_update:0:attributes:p_R|H2:regular": [0.2879516943574264, 0.2879516943574264,
                                                                    0.1934854687712363, 0.1934854687712363,
                                                                    0.16519150276857708, 0.16519150276857708,
                                                                    0.15122036774294778, 0.15122036774294778,
                                                                    0.11494251917311381, 0.11494251917311381,
                                                                    0.08159978080408017, 0.08159978080408017,
                                                                    0.040103340075634965, 0.040103340075634965,
                                                                    0.035140865119678066, 0.035140865119678066,
                                                                    0.029618555795599157],
            "COVID:attributes_update:0:attributes:p_D|H2:regular": [0.0, 0.0, 0.00030866413855605855,
                                                                    0.00030866413855605855, 0.0009881000949241944,
                                                                    0.0009881000949241944, 0.001, 0.001,
                                                                    0.0018023862150114574, 0.0018023862150114574,
                                                                    0.0028909696546907454, 0.0028909696546907454,
                                                                    0.008547087001146626, 0.008547087001146626,
                                                                    0.011754201172328317, 0.011754201172328317,
                                                                    0.015932531419830247],
            "COVID:attributes_update:0:attributes:p_R|H3:regular": [0.08364763303255095, 0.08364763303255095,
                                                                    0.05173966316437428, 0.05173966316437428,
                                                                    0.21079942114437247, 0.21079942114437247,
                                                                    0.05731331434085982, 0.05731331434085982,
                                                                    0.05535440731053916, 0.05535440731053916,
                                                                    0.05292626611039884, 0.05292626611039884,
                                                                    0.10166837673834904, 0.10166837673834904,
                                                                    0.05893807240160405, 0.05893807240160405,
                                                                    0.07260548142691702],
            "COVID:attributes_update:0:attributes:p_D|H3:regular": [1.450214372862469e-06, 1.450214372862469e-06,
                                                                    0.0, 0.0, 0.001029394057065291,
                                                                    0.001029394057065291, 0.004704137941169275,
                                                                    0.004704137941169275, 0.004332116876854601,
                                                                    0.004332116876854601, 0.007196263810759117,
                                                                    0.007196263810759117, 0.001329313930006542,
                                                                    0.001329313930006542, 0.003682029009843512,
                                                                    0.003682029009843512, 0.013914482864569564],
            "COVID:attributes_update:0:attributes:p_H2|H1:B.1.1.7": [0.9479088828090996, 0.9479088828090996,
                                                                     0.966777512062082, 0.966777512062082,
                                                                     0.9018353444164374, 0.9018353444164374,
                                                                     0.9183248612317887, 0.9183248612317887,
                                                                     0.8730129124064093, 0.8730129124064093,
                                                                     0.8413026634950577, 0.8413026634950577,
                                                                     0.39582793927434223, 0.39582793927434223,
                                                                     0.7323245088965133, 0.7323245088965133,
                                                                     0.8946574656948076],
            "COVID:attributes_update:0:attributes:p_ICU|H1:B.1.1.7": [0.05209111719090043, 0.05209111719090043,
                                                                      0.03073764532567787, 0.03073764532567787,
                                                                      0.076998949309656, 0.076998949309656,
                                                                      0.07052315993853264, 0.07052315993853264,
                                                                      0.11574303671502471, 0.11574303671502471,
                                                                      0.1571068344887177, 0.1571068344887177,
                                                                      0.2797592947742128, 0.2797592947742128,
                                                                      0.26767549110348665, 0.26767549110348665,
                                                                      0.09833221081844357],
            "COVID:attributes_update:0:attributes:p_H3|ICU:B.1.1.7": [0.07928153703022643, 0.07928153703022643,
                                                                      0.03333333333333333, 0.03333333333333333,
                                                                      0.07006318123621075, 0.07006318123621075,
                                                                      0.03333333333333333, 0.03333333333333333,
                                                                      0.03333333333333333, 0.03333333333333333,
                                                                      0.03333333333333333, 0.03333333333333333,
                                                                      0.07534078822104602, 0.07534078822104602,
                                                                      0.05682056530413365, 0.05682056530413365,
                                                                      0.13932869597489814],
            "COVID:attributes_update:0:attributes:p_R|H2:B.1.1.7": [0.2879516943574264, 0.2879516943574264,
                                                                    0.1934854687712363, 0.1934854687712363,
                                                                    0.16519150276857708, 0.16519150276857708,
                                                                    0.15122036774294778, 0.15122036774294778,
                                                                    0.11494251917311381, 0.11494251917311381,
                                                                    0.08159978080408017, 0.08159978080408017,
                                                                    0.040103340075634965, 0.040103340075634965,
                                                                    0.035140865119678066, 0.035140865119678066,
                                                                    0.029618555795599157],
            "COVID:attributes_update:0:attributes:p_D|H2:B.1.1.7": [0.0, 0.0, 0.00030866413855605855,
                                                                    0.00030866413855605855, 0.0009881000949241944,
                                                                    0.0009881000949241944, 0.001, 0.001,
                                                                    0.0018023862150114574, 0.0018023862150114574,
                                                                    0.0028909696546907454, 0.0028909696546907454,
                                                                    0.008547087001146626, 0.008547087001146626,
                                                                    0.011754201172328317, 0.011754201172328317,
                                                                    0.015932531419830247],
            "COVID:attributes_update:0:attributes:p_R|H3:B.1.1.7": [0.08364763303255095, 0.08364763303255095,
                                                                    0.05173966316437428, 0.05173966316437428,
                                                                    0.21079942114437247, 0.21079942114437247,
                                                                    0.05731331434085982, 0.05731331434085982,
                                                                    0.05535440731053916, 0.05535440731053916,
                                                                    0.05292626611039884, 0.05292626611039884,
                                                                    0.10166837673834904, 0.10166837673834904,
                                                                    0.05893807240160405, 0.05893807240160405,
                                                                    0.07260548142691702],
            "COVID:attributes_update:0:attributes:p_D|H3:B.1.1.7": [1.450214372862469e-06, 1.450214372862469e-06,
                                                                    0.0, 0.0, 0.001029394057065291,
                                                                    0.001029394057065291, 0.004704137941169275,
                                                                    0.004704137941169275, 0.004332116876854601,
                                                                    0.004332116876854601, 0.007196263810759117,
                                                                    0.007196263810759117, 0.001329313930006542,
                                                                    0.001329313930006542, 0.003682029009843512,
                                                                    0.003682029009843512, 0.013914482864569564]
        }
        loop_dicts.append(new_parameters)
    return loop_dicts


def historical_variant_strains_with_vaccination():
    """
    Experimentation for:
    - vaccine
    - variant with:
        - two vaccination strategies: all population vaccinated; only 10+ vaccinated
        - 3 reduction of virus transmission: 50%, 75%, 90%
        - 4 vaccination campaign duration: 6 months, 9 months, 1 year, 18months
    :return: an array of simulation parameters
    """
    print('Start an experimentation with {}'.format(inspect.currentframe().f_code.co_name))
    loop_dicts = []
    beta1 = 0.043
    # Extended is the beta2 vector of values with the november reduction
    # Intensive is the beta2 vector of values with the december/january reduction
    beta2 = {'extended': [0.14263832148900557,
                          0.14263832148900557,
                          0.14263832148900557,
                          0.14263832148900557,
                          0.21684912145983778,
                          0.21684912145983778,
                          0.09512233780076533,
                          0.09512233780076533,
                          0.043417796014587506,
                          0.1306010764544347,
                          0.12247917509746761,
                          0.18306554283556445,
                          0.19463630519256617,
                          0.19463630519256617,
                          1.9907690993899558,
                          1.9907690993899558,
                          2.0393399666307706],
             'intensive': [0.2089935922949808,
                           0.2089935922949808,
                           0.2089935922949808,
                           0.2089935922949808,
                           0.3177272166890673,
                           0.3177272166890673,
                           0.13937319842908358,
                           0.13937319842908358,
                           0.06361573148011815,
                           0.19135662731356243,
                           0.179456421792784,
                           0.2682275353722829,
                           0.28518101018422337,
                           0.28518101018422337,
                           2.9168738188174763,
                           2.9168738188174763,
                           2.9880398274999527],
             'relaxed': [0.26688357961109804,
                         0.26688357961109804,
                         0.26688357961109804,
                         0.26688357961109804,
                         0.4057357739952382,
                         0.4057357739952382,
                         0.17797874896614715,
                         0.17797874896614715,
                         0.08123691234049424,
                         0.2443612797834896,
                         0.22916478781157104,
                         0.34252497411202437,
                         0.3641744610411312,
                         0.3641744610411312,
                         3.7248305916534896,
                         3.7248305916534896,
                         3.815709163265419]}
    r_effective = {'extended': 0.525, 'intensive': 0.773, 'relaxed': 1}
    campaign_durations = [180, 270, 360, 540]
    priorities_age = {"0": [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], "1": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]}
    transmission_reductions = [0.5, 0.75, 0.9]
    iteration_shift_probability_right_side = [183]
    beta3 = 1.57212382  # New variant relative infectiousness
    pop_reduction_factor = 0.02
    scenarios = {
        "1wave_intensive": {
            "0": {
                "T_b": 0,
                "T_e": 59,
                "beta2_t1": beta2['relaxed'],
                "R_0": r_effective['relaxed']
            },
            "1": {
                "T_b": 59,
                "T_e": 92,
                "beta2_t1": beta2['extended'],
                "R_0": r_effective['extended']
            },
            "2": {
                "T_b": 92,
                "T_e": 122,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            },
            "3": {
                "T_b": 122,
                "T_e": 675,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            }
        },
        "1wave_extended": {
            "0": {
                "T_b": 0,
                "T_e": 59,
                "beta2_t1": beta2['relaxed'],
                "R_0": r_effective['relaxed']
            },
            "1": {
                "T_b": 59,
                "T_e": 92,
                "beta2_t1": beta2['extended'],
                "R_0": r_effective['extended']
            },
            "2": {
                "T_b": 92,
                "T_e": 122,
                "beta2_t1": beta2['intensive'],
                "R_0": r_effective['intensive']
            },
            "3": {
                "T_b": 122,
                "T_e": 675,
                "beta2_t1": beta2['extended'],
                "R_0": r_effective['extended']
            }
        }
    }
    for scenario_name, dict_scenario in scenarios.items():
        for duration in campaign_durations:
            for priority_index, priority_age in priorities_age.items():
                for transmission_reduction in transmission_reductions:
                    new_parameters = {
                        "simulation:folder_name":
                            "CovDyn_Model_vaccine_reduction_2p/{}_{}_{}_{}".format(
                                duration,
                                scenario_name,
                                priority_index,
                                str(transmission_reduction).replace('.', '')
                                ),
                        "simulation:simulation_duration": 675,
                        "simulation:calibration": 0,
                        "simulation:variant": ['regular', 'B.1.1.7'],
                        "simulation:loop_number": 50,
                        "vaccination:available": 1,
                        "population:population_distribution:reduction_factor": pop_reduction_factor,
                        "COVID:initial_compartment_per_age:regular:details:percentage_removed": 0.08,
                        "COVID:initial_compartment_per_age:regular:targets:prevalence:hospitalized": [
                            1,
                            1,
                            1,
                            2,
                            12,
                            13,
                            13,
                            13,
                            12,
                            13,
                            13,
                            13,
                            23,
                            23,
                            19,
                            20,
                            73],
                        "COVID:initial_compartment_per_age:regular:targets:prevalence:hospitalizedSecond": [
                            6,
                            6,
                            4,
                            4,
                            14,
                            13,
                            21,
                            21,
                            48,
                            48,
                            104,
                            104,
                            188,
                            188,
                            248,
                            247,
                            775],
                        "COVID:initial_compartment_per_age:regular:targets:prevalence:hospitalizedThird": [
                            0,
                            0,
                            4,
                            4,
                            14,
                            13,
                            21,
                            21,
                            48,
                            48,
                            104,
                            104,
                            188,
                            188,
                            248,
                            247,
                            775],
                        "COVID:initial_compartment_per_age:regular:targets:prevalence:ICU": [
                            1,
                            2,
                            0,
                            1,
                            3,
                            4,
                            7,
                            7,
                            13,
                            14,
                            26,
                            26,
                            53,
                            53,
                            56,
                            57,
                            54],
                        "COVID:initial_compartment_per_age:regular:targets:incidence:hospitalized": [
                            1,
                            1,
                            1,
                            2,
                            12,
                            13,
                            13,
                            13,
                            12,
                            13,
                            13,
                            13,
                            23,
                            23,
                            19,
                            20,
                            73],
                        "COVID:initial_compartment_per_age:B.1.1.7:details:percentage_intercept": 0.033,
                        "COVID:initial_compartment_per_age:B.1.1.7:details:start": 131,
                        "COVID:model:CovDyn_Model:attributes:p_A:regular": 0.2,
                        "COVID:model:CovDyn_Model:attributes:p_A:B.1.1.7": 0.2,
                        "COVID:model:CovDyn_Model:attributes:beta1:regular": beta1,
                        "COVID:model:CovDyn_Model:attributes:beta1:B.1.1.7": beta1,
                        "COVID:model:CovDyn_Model:sanitary_update": dict_scenario,
                        "COVID:model:CovDyn_Model:attributes:i_A:B.1.1.7": 0.55 * beta3,
                        "COVID:model:CovDyn_Model:attributes:i_Ip:B.1.1.7": 0.55 * beta3,
                        "COVID:model:CovDyn_Model:attributes:i_Ips:B.1.1.7": 0.55 * beta3,
                        "COVID:model:CovDyn_Model:attributes:i_Ims:B.1.1.7": 1 * beta3,
                        "COVID:model:CovDyn_Model:attributes:i_Iss:B.1.1.7": 1 * beta3,
                        "COVID:attributes_update:iterations": iteration_shift_probability_right_side,
                        "vaccination:campaign_duration": duration,
                        "vaccination:strategy:age_based:priority:3:age": priority_age,
                        "vaccination:vaccine_type:Pfizer:attributes_first_injection:i_A:regular": 0.55 * (
                                1 - transmission_reduction),
                        "vaccination:vaccine_type:Pfizer:attributes_first_injection:i_A:B.1.1.7": 0.55 * beta3 * (
                                1 - transmission_reduction),
                        "vaccination:vaccine_type:Pfizer:attributes_first_injection:i_Ip:regular": 0.55 * (
                                1 - transmission_reduction),
                        "vaccination:vaccine_type:Pfizer:attributes_first_injection:i_Ip:B.1.1.7": 0.55 * beta3 * (
                                1 - transmission_reduction),
                        "vaccination:vaccine_type:Pfizer:attributes_first_injection:i_Ims:regular": 1 * (
                                1 - transmission_reduction),
                        "vaccination:vaccine_type:Pfizer:attributes_first_injection:i_Ims:B.1.1.7": 1 * beta3 * (
                                1 - transmission_reduction),
                        "vaccination:vaccine_type:Pfizer:attributes_first_injection:i_Ips:regular": 0.55 * (
                                1 - transmission_reduction),
                        "vaccination:vaccine_type:Pfizer:attributes_first_injection:i_Ips:B.1.1.7": 0.55 * beta3 * (
                                1 - transmission_reduction),
                        "vaccination:vaccine_type:Pfizer:attributes_first_injection:i_Iss:regular": 1 * (
                                1 - transmission_reduction),
                        "vaccination:vaccine_type:Pfizer:attributes_first_injection:i_Iss:B.1.1.7": 1 * beta3 * (
                                1 - transmission_reduction),
                        "vaccination:vaccine_type:AstraZeneca:attributes_first_injection:i_A:regular": 0.55 * (
                                1 - transmission_reduction),
                        "vaccination:vaccine_type:AstraZeneca:attributes_first_injection:i_A:B.1.1.7": 0.55 * beta3 * (
                                1 - transmission_reduction),
                        "vaccination:vaccine_type:AstraZeneca:attributes_first_injection:i_Ip:regular": 0.55 * (
                                1 - transmission_reduction),
                        "vaccination:vaccine_type:AstraZeneca:attributes_first_injection:i_Ip:B.1.1.7": 0.55 * beta3 * (
                                1 - transmission_reduction),
                        "vaccination:vaccine_type:AstraZeneca:attributes_first_injection:i_Ims:regular": 1 * (
                                1 - transmission_reduction),
                        "vaccination:vaccine_type:AstraZeneca:attributes_first_injection:i_Ims:B.1.1.7": 1 * beta3 * (
                                1 - transmission_reduction),
                        "vaccination:vaccine_type:AstraZeneca:attributes_first_injection:i_Ips:regular": 0.55 * (
                                1 - transmission_reduction),
                        "vaccination:vaccine_type:AstraZeneca:attributes_first_injection:i_Ips:B.1.1.7":
                            0.55 * beta3 * (1 - transmission_reduction),
                        "vaccination:vaccine_type:AstraZeneca:attributes_first_injection:i_Iss:regular": 1 * (
                                1 - transmission_reduction),
                        "vaccination:vaccine_type:AstraZeneca:attributes_first_injection:i_Iss:B.1.1.7": 1 * beta3 * (
                                1 - transmission_reduction),
                        "vaccination:strategy:age_based:priority:0:start": 0,
                        "vaccination:strategy:age_based:priority:1:start": 0,
                        "vaccination:strategy:age_based:priority:2:start": 0,
                        "vaccination:strategy:age_based:priority:3:start": 0,
                        "COVID:attributes_update:0:attributes:p_H2|H1:regular": [0.9479088828090996,
                                                                                 0.9479088828090996,
                                                                                 0.966777512062082,
                                                                                 0.966777512062082,
                                                                                 0.9018353444164374,
                                                                                 0.9018353444164374,
                                                                                 0.9183248612317887,
                                                                                 0.9183248612317887,
                                                                                 0.8730129124064093,
                                                                                 0.8730129124064093,
                                                                                 0.8413026634950577,
                                                                                 0.8413026634950577,
                                                                                 0.39582793927434223,
                                                                                 0.39582793927434223,
                                                                                 0.7323245088965133,
                                                                                 0.7323245088965133,
                                                                                 0.8946574656948076],
                        "COVID:attributes_update:0:attributes:p_ICU|H1:regular": [0.05209111719090043,
                                                                                  0.05209111719090043,
                                                                                  0.03073764532567787,
                                                                                  0.03073764532567787,
                                                                                  0.076998949309656,
                                                                                  0.076998949309656,
                                                                                  0.07052315993853264,
                                                                                  0.07052315993853264,
                                                                                  0.11574303671502471,
                                                                                  0.11574303671502471,
                                                                                  0.1571068344887177,
                                                                                  0.1571068344887177,
                                                                                  0.2797592947742128,
                                                                                  0.2797592947742128,
                                                                                  0.26767549110348665,
                                                                                  0.26767549110348665,
                                                                                  0.09833221081844357],
                        "COVID:attributes_update:0:attributes:p_H3|ICU:regular": [0.07928153703022643,
                                                                                  0.07928153703022643,
                                                                                  0.03333333333333333,
                                                                                  0.03333333333333333,
                                                                                  0.07006318123621075,
                                                                                  0.07006318123621075,
                                                                                  0.03333333333333333,
                                                                                  0.03333333333333333,
                                                                                  0.03333333333333333,
                                                                                  0.03333333333333333,
                                                                                  0.03333333333333333,
                                                                                  0.03333333333333333,
                                                                                  0.07534078822104602,
                                                                                  0.07534078822104602,
                                                                                  0.05682056530413365,
                                                                                  0.05682056530413365,
                                                                                  0.13932869597489814],
                        "COVID:attributes_update:0:attributes:p_R|H2:regular": [0.2879516943574264,
                                                                                0.2879516943574264,
                                                                                0.1934854687712363,
                                                                                0.1934854687712363,
                                                                                0.16519150276857708,
                                                                                0.16519150276857708,
                                                                                0.15122036774294778,
                                                                                0.15122036774294778,
                                                                                0.11494251917311381,
                                                                                0.11494251917311381,
                                                                                0.08159978080408017,
                                                                                0.08159978080408017,
                                                                                0.040103340075634965,
                                                                                0.040103340075634965,
                                                                                0.035140865119678066,
                                                                                0.035140865119678066,
                                                                                0.029618555795599157],
                        "COVID:attributes_update:0:attributes:p_D|H2:regular": [0.0, 0.0, 0.00030866413855605855,
                                                                                0.00030866413855605855,
                                                                                0.0009881000949241944,
                                                                                0.0009881000949241944, 0.001, 0.001,
                                                                                0.0018023862150114574,
                                                                                0.0018023862150114574,
                                                                                0.0028909696546907454,
                                                                                0.0028909696546907454,
                                                                                0.008547087001146626,
                                                                                0.008547087001146626,
                                                                                0.011754201172328317,
                                                                                0.011754201172328317,
                                                                                0.015932531419830247],
                        "COVID:attributes_update:0:attributes:p_R|H3:regular": [0.08364763303255095,
                                                                                0.08364763303255095,
                                                                                0.05173966316437428,
                                                                                0.05173966316437428,
                                                                                0.21079942114437247,
                                                                                0.21079942114437247,
                                                                                0.05731331434085982,
                                                                                0.05731331434085982,
                                                                                0.05535440731053916,
                                                                                0.05535440731053916,
                                                                                0.05292626611039884,
                                                                                0.05292626611039884,
                                                                                0.10166837673834904,
                                                                                0.10166837673834904,
                                                                                0.05893807240160405,
                                                                                0.05893807240160405,
                                                                                0.07260548142691702],
                        "COVID:attributes_update:0:attributes:p_D|H3:regular": [1.450214372862469e-06,
                                                                                1.450214372862469e-06, 0.0, 0.0,
                                                                                0.001029394057065291,
                                                                                0.001029394057065291,
                                                                                0.004704137941169275,
                                                                                0.004704137941169275,
                                                                                0.004332116876854601,
                                                                                0.004332116876854601,
                                                                                0.007196263810759117,
                                                                                0.007196263810759117,
                                                                                0.001329313930006542,
                                                                                0.001329313930006542,
                                                                                0.003682029009843512,
                                                                                0.003682029009843512,
                                                                                0.013914482864569564],
                        "COVID:attributes_update:0:attributes:p_H2|H1:B.1.1.7": [0.9479088828090996,
                                                                                 0.9479088828090996,
                                                                                 0.966777512062082,
                                                                                 0.966777512062082,
                                                                                 0.9018353444164374,
                                                                                 0.9018353444164374,
                                                                                 0.9183248612317887,
                                                                                 0.9183248612317887,
                                                                                 0.8730129124064093,
                                                                                 0.8730129124064093,
                                                                                 0.8413026634950577,
                                                                                 0.8413026634950577,
                                                                                 0.39582793927434223,
                                                                                 0.39582793927434223,
                                                                                 0.7323245088965133,
                                                                                 0.7323245088965133,
                                                                                 0.8946574656948076],
                        "COVID:attributes_update:0:attributes:p_ICU|H1:B.1.1.7": [0.05209111719090043,
                                                                                  0.05209111719090043,
                                                                                  0.03073764532567787,
                                                                                  0.03073764532567787,
                                                                                  0.076998949309656,
                                                                                  0.076998949309656,
                                                                                  0.07052315993853264,
                                                                                  0.07052315993853264,
                                                                                  0.11574303671502471,
                                                                                  0.11574303671502471,
                                                                                  0.1571068344887177,
                                                                                  0.1571068344887177,
                                                                                  0.2797592947742128,
                                                                                  0.2797592947742128,
                                                                                  0.26767549110348665,
                                                                                  0.26767549110348665,
                                                                                  0.09833221081844357],
                        "COVID:attributes_update:0:attributes:p_H3|ICU:B.1.1.7": [0.07928153703022643,
                                                                                  0.07928153703022643,
                                                                                  0.03333333333333333,
                                                                                  0.03333333333333333,
                                                                                  0.07006318123621075,
                                                                                  0.07006318123621075,
                                                                                  0.03333333333333333,
                                                                                  0.03333333333333333,
                                                                                  0.03333333333333333,
                                                                                  0.03333333333333333,
                                                                                  0.03333333333333333,
                                                                                  0.03333333333333333,
                                                                                  0.07534078822104602,
                                                                                  0.07534078822104602,
                                                                                  0.05682056530413365,
                                                                                  0.05682056530413365,
                                                                                  0.13932869597489814],
                        "COVID:attributes_update:0:attributes:p_R|H2:B.1.1.7": [0.2879516943574264,
                                                                                0.2879516943574264,
                                                                                0.1934854687712363,
                                                                                0.1934854687712363,
                                                                                0.16519150276857708,
                                                                                0.16519150276857708,
                                                                                0.15122036774294778,
                                                                                0.15122036774294778,
                                                                                0.11494251917311381,
                                                                                0.11494251917311381,
                                                                                0.08159978080408017,
                                                                                0.08159978080408017,
                                                                                0.040103340075634965,
                                                                                0.040103340075634965,
                                                                                0.035140865119678066,
                                                                                0.035140865119678066,
                                                                                0.029618555795599157],
                        "COVID:attributes_update:0:attributes:p_D|H2:B.1.1.7": [0.0, 0.0, 0.00030866413855605855,
                                                                                0.00030866413855605855,
                                                                                0.0009881000949241944,
                                                                                0.0009881000949241944, 0.001, 0.001,
                                                                                0.0018023862150114574,
                                                                                0.0018023862150114574,
                                                                                0.0028909696546907454,
                                                                                0.0028909696546907454,
                                                                                0.008547087001146626,
                                                                                0.008547087001146626,
                                                                                0.011754201172328317,
                                                                                0.011754201172328317,
                                                                                0.015932531419830247],
                        "COVID:attributes_update:0:attributes:p_R|H3:B.1.1.7": [0.08364763303255095,
                                                                                0.08364763303255095,
                                                                                0.05173966316437428,
                                                                                0.05173966316437428,
                                                                                0.21079942114437247,
                                                                                0.21079942114437247,
                                                                                0.05731331434085982,
                                                                                0.05731331434085982,
                                                                                0.05535440731053916,
                                                                                0.05535440731053916,
                                                                                0.05292626611039884,
                                                                                0.05292626611039884,
                                                                                0.10166837673834904,
                                                                                0.10166837673834904,
                                                                                0.05893807240160405,
                                                                                0.05893807240160405,
                                                                                0.07260548142691702],
                        "COVID:attributes_update:0:attributes:p_D|H3:B.1.1.7": [1.450214372862469e-06,
                                                                                1.450214372862469e-06, 0.0, 0.0,
                                                                                0.001029394057065291,
                                                                                0.001029394057065291,
                                                                                0.004704137941169275,
                                                                                0.004704137941169275,
                                                                                0.004332116876854601,
                                                                                0.004332116876854601,
                                                                                0.007196263810759117,
                                                                                0.007196263810759117,
                                                                                0.001329313930006542,
                                                                                0.001329313930006542,
                                                                                0.003682029009843512,
                                                                                0.003682029009843512,
                                                                                0.013914482864569564]
                    }
                    loop_dicts.append(new_parameters)
    return loop_dicts
