#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By : Simon Pageaud
# License    : BSD-3-Clause Copyright 2021, CovDyn Developers.
# =============================================================================
"""
This module contains an abstract description of a SEIR model for different
implementation. Only one implementation is available: CovDynModel.
"""
# =============================================================================
# Imports
# =============================================================================
import file_manipulation
import global_variables
from global_variables import GlobalVar
from vaccine import Vaccine
from variant import Variant

import pandas as pd
import numpy as np
import random as rnd
from typing import NamedTuple
from math import floor


class Mask(NamedTuple):
    name: str
    inward: float
    outward: float
    distribution: float
    index: int


class SEIRModel:
    def __init__(self, parameters, age_range=1):
        ts = parameters['time_step']
        if age_range:
            age_distribution_global = parameters['population_per_age']
            reduction_factor = parameters['reduction_factor']
            self.age_distribution = []
            for i in range(len(age_distribution_global)):
                self.age_distribution.append(floor(reduction_factor * age_distribution_global[i]))
            self.total_population = sum(self.age_distribution)

            self.contact_matrix = file_manipulation.read_csv('../input/' + parameters['CM_file'])
            GlobalVar.Instance().average_global_contacts = self.mean_number_of_contacts(self.age_distribution,
                                                                                        self.contact_matrix)
            self.contact_matrix_per_agent = self.create_contact_matrix(self.age_distribution, self.contact_matrix)
            self.probabilities_per_age = self.compute_probabilities_to_draw_contacts(self.contact_matrix)
            self.proportions_of_contacts_per_age = self.create_contacts_per_age(self.contact_matrix)
        else:
            print('Error in age selection.')

        # Csv related attributes
        self.age_ranges = parameters['age_range']
        self.short_comp_names = parameters['short_comp_names']

        # Variant attributes
        self.variants = {}
        for index, variant in enumerate(parameters["variant"]):
            if len(parameters['compartments_per_age'][variant]) == 0:
                initial_compartments = None
            else:
                initial_compartments = parameters['compartments_per_age'][variant]
            self.variants[variant] = Variant(self, parameters, index, variant, initial_compartments)
            GlobalVar.Instance().variants[variant] = self.variants[variant]
        self.default_variant = "regular"

        self.draw_again = parameters['infection_only_susceptible']
        self.infection_type = parameters['infection_type']
        if 'transition_state' in parameters:
            self.transition_type = self.create_probability_distribution(parameters)
        if GlobalVar.Instance().vaccination:
            self.default_vaccine = parameters['vaccination_parameters']['available_vaccines'][0]
            # Dictionary for vaccine related attributes used in function vaccine_distribution
            GlobalVar.Instance().first_shots_per_iteration, GlobalVar.Instance().second_shots_per_iteration = \
                global_variables.compute_new_agents_vaccinated_per_iteration(parameters, self.age_distribution)
            vac_prm = parameters['vaccination_parameters']
            self.vaccine_attributes = {'success_rate': vac_prm['success_rate'],
                                       'priority': {},  # Index of priority and list of age range for this priority
                                       'vaccines_per_priority': {},  # dict of dict
                                       # Index of priority and list of available vaccines per priority
                                       'ignored_compartments': vac_prm['ignored_compartments'],
                                       'daily_capacity': vac_prm['daily_capacity'],
                                       'remaining_vaccine': 0,
                                       'start_iteration': int(vac_prm['start_iteration'])}
            vaccine_strategy = vac_prm['vaccine_strategy']
            priority_strategy = vac_prm['strategy'][vaccine_strategy]['priority']
            self.vaccines = {}
            for index, vaccine in enumerate(vac_prm['available_vaccines']):
                self.vaccines[vaccine] = Vaccine(vaccine, vac_prm['vaccine_type'][vaccine], ts)
            GlobalVar.Instance().vaccines = self.vaccines
            self.vaccinated_agents = []
            self.first_shot_agents = []
            self.second_shot_agents = []
            for i in range(parameters['simulation_duration']):
                self.first_shot_agents.append([])
                GlobalVar.Instance().success_agents_vaccinated_per_iteration.append([])
            if len(priority_strategy) == 1 and '-1' in priority_strategy:
                # Default value. Adds every age range in the priority
                self.vaccine_attributes['priority'][0] = []
                self.vaccine_attributes['vaccines_per_priority'][0] = {
                    'vaccines': priority_strategy[-1]['available_vaccines'],
                    'age': {}}
                for age_range, agents_per_age in enumerate(vac_prm['wants_vaccine_per_age']):
                    self.vaccine_attributes['priority'][0].append(age_range)
            else:
                # Creates a priority of vaccination
                for priority, priority_values in priority_strategy.items():
                    priority = int(priority)
                    self.vaccine_attributes['vaccines_per_priority'][priority] = {
                        'vaccines': priority_values['available_vaccines'],
                        'age': {}}
                    if len(priority_values['available_vaccines']) > 1:
                        assert 'proportion_vaccine_per_age' in priority_values
                        for age_index, proportion_of_vaccine_per_age in priority_values[
                            'proportion_vaccine_per_age'].items():
                            self.vaccine_attributes['vaccines_per_priority'][priority]['age'][age_index] = proportion_of_vaccine_per_age

                    if priority != -1:  # Default value, doesn't have an age attribute
                        self.vaccine_attributes['priority'][priority] = priority_values['age']
                    else:
                        self.vaccine_attributes['priority'][priority] = []
            for i in range(len(vac_prm['wants_vaccine_per_age'])):
                GlobalVar.Instance().first_shot_agents_per_age.append(0)
                GlobalVar.Instance().second_shot_agents_per_age.append(0)
                GlobalVar.Instance().vaccinated_agents_per_age.append(0)

    def live(self):
        """
        Stores information of the simulation. Called once per iteration by population.live()
        :return:
        """
        pass

    def update_variant(self, parameters):
        # Variant attributes
        self.variants = {}
        for index, variant in enumerate(parameters["variant"]):
            if len(parameters['compartments_per_age'][variant]) == 0:
                initial_compartments = None
            else:
                initial_compartments = parameters['compartments_per_age'][variant]
            self.variants[variant] = Variant(self, parameters, index, variant, initial_compartments)
        GlobalVar.Instance().variants = self.variants

    def create_columns_for_csv(self):
        """
        Creates the layout of the output csv file.
        The csv file requires:
        - column names
        - age range if available
        :return: A MultiIndex from pandas library
        """
        # https://stackoverflow.com/questions/22356746/how-can-i-write-a-csv-file-with-multiple-header-lines-with-pandas-to-csv/26815898
        pass

    def create_population(self, parameters, population):
        """
        Instantiates every agent of the simulation to the default compartment (here susceptible).
        """
        pass

    def create_compartments(self, parameters, population):
        """
        Changes compartment of susceptible to match 'initial_compartment' of the json.
        :return:
        """
        pass

    def create_initial_compartments_old(self, age_distribution, incidence_rate, agent_per_day,
                                        percentage_removed, compartments, non_age_dependant_attributes,
                                        age_dependant_attributes):
        """
        Initializes each compartment with agents at the first iteration.
        :param age_distribution:
        :param incidence_rate:
        :param agent_per_day:
        :param percentage_removed:
        :param compartments:
        :param non_age_dependant_attributes:
        :param age_dependant_attributes:
        :return: dict of agents per compartments at t=0
        """

    def create_initial_compartments(self, age_distribution, initialization_values, non_age_dependant_attributes,
                                    age_dependant_attributes):
        """
        Initializes each compartment with agents at the first iteration.
        :param age_distribution:
        :param initialization_values:
        :param non_age_dependant_attributes:
        :param age_dependant_attributes:
        :return: dict of agents per compartments at t=0
        """

    def create_initial_compartments_during_simulation(self, intercept, base_variant, compartments_to_fill):
        """
        Sets a number of agents per age in each specified compartments with an intercept of the given base_variant.
        Then change state of susceptible agents per age to the new variant
        :param compartments_to_fill:
        :param intercept:
        :param base_variant:
        :return: a dictionary of compartments and list of agents per age in this compartment
        """

    def update_compartments_during_simulation(self, current_variant, initial_compartments, population):
        """
        Updates compartments of susceptible agents during simulation
        :param population:
        :param current_variant: variant to update
        :param initial_compartments:
        :return:
        """

    @staticmethod
    def create_probability_distribution(parameters):
        """
        Describes the state transition probability function.
        :param parameters:
        :return:
        """
        if parameters['transition_state']['name'] == 'Geometric':
            pass
        elif parameters['transition_state']['name'] == 'Weibull':
            GlobalVar.Instance().transition_type = 'Weibull'
            return {'k': parameters['transition_state']['k']}

    @staticmethod
    def mean_number_of_contacts(age_distribution, contact_matrix):
        weighted_sum = 0
        for index, agents_per_age in enumerate(age_distribution, start=0):
            weighted_sum += sum(contact_matrix.values[index][1:]) * age_distribution[index]
        return weighted_sum / sum(age_distribution)

    @staticmethod
    def create_contact_matrix(age_distribution, contact_matrix):
        """
        Attribute a number of contacts based on a poisson law for a specific age
        and the given matrix contact
        :return: A list of number of contacts (list of integers)
        """
        contact_agents = []
        for index, agents_per_age in enumerate(age_distribution, start=0):
            # Index of age, number of agents in this age then sum of contacts
            contacts_per_agent = np.random.poisson(sum(contact_matrix.values[index][1:]), agents_per_age)
            contact_agents.append(contacts_per_agent)
        return contact_agents

    @staticmethod
    def compute_probabilities_to_draw_contacts(contact_matrix):
        """
        Based on the importance of each age encounter, draw a number of contacts for an agent.
        :return: The probability to meet an agent from the specific age range.
        """
        # Stores an array of probabilities
        sum_of_contacts_per_age = [sum(contact_matrix.values[row][1:]) for row in
                                   range(len(contact_matrix.values))]
        probabilities_per_age = [contact_matrix.values[row][1:] / sum_of_contacts_per_age[row]
                                 for row in range(len(sum_of_contacts_per_age))]

        cumulated_probabilities_per_age = []
        for i in range(len(probabilities_per_age)):
            cumulated_for_age = np.array([])
            previous_probability = 0.0
            for j in range(len(probabilities_per_age[i])):
                cumulated_for_age = np.append(cumulated_for_age, previous_probability + probabilities_per_age[i][j])
                previous_probability += probabilities_per_age[i][j]
            cumulated_probabilities_per_age.append(cumulated_for_age)
        return cumulated_probabilities_per_age

    @staticmethod
    def create_contacts_per_age(contact_matrix):
        """
        Creates an array of 1000 integers based on the proportion of each age range for one age.
        :return:
        """
        sum_of_contacts_per_age = [sum(contact_matrix.values[row][1:]) for row in
                                   range(len(contact_matrix.values))]
        proportion_per_age = [contact_matrix.values[row][1:] / sum_of_contacts_per_age[row]
                              for row in range(len(sum_of_contacts_per_age))]
        proportion_of_agents_per_age = []
        for age_range in proportion_per_age:
            current_proportion = []
            for age_index, proportion in enumerate(age_range):
                number_of_individuals = round(proportion * 1000)
                for i in range(0, number_of_individuals):
                    current_proportion.append(age_index)
            proportion_of_agents_per_age.append(current_proportion)
        return proportion_of_agents_per_age

    def create_multiple_columns(self, calibration=False):
        """
        Creates csv files format to store simulation modifications over time.
        Adds columns based on short compartment names and considered age ranges.
        :return: two lists of csv columns and csv file names
        """
        files_names = []
        columns_final = []

        if not calibration:
            # File for vaccine per iteration per age
            if GlobalVar.Instance().vaccination:
                files_names.append('vaccines')
                vaccine_column_name = ['first_shot', 'second_shot']
                columns_final.append(self.create_columns_for_csv_with(vaccine_column_name, self.age_ranges))

            # File for cumulated entries in H and ICU
            files_names.append('cumulative_hospital')
            names = ['H', 'ICU']
            cumulated_entries_columns = [i + '_' + j for i in names for j, v in GlobalVar.Instance().variants.items()]
            columns_final.append(self.create_columns_for_csv_with(cumulated_entries_columns, self.age_ranges))

            # File for ins and outs of agents per iteration
            files_names.append('ins')
            files_names.append('outs')
            columns_ins = [i + '_' + j for i in self.short_comp_names for j, v in GlobalVar.Instance().variants.items()]
            columns_outs = [i + '_' + j for i in self.short_comp_names for j, v in
                            GlobalVar.Instance().variants.items()]
            columns_final.append(self.create_columns_for_csv_with(columns_ins, self.age_ranges))
            columns_final.append(self.create_columns_for_csv_with(columns_outs, self.age_ranges))
        else:
            # Adds files used only for calibration purpose
            files_names.append('calibration_prevalence')
            names = ['H1', 'H2', 'H3', 'ICU', 'D', 'R']  # Removed from Hospital only
            ages_range_calibration = []
            for i in range(0, 8):
                ages_range_calibration.append('{}-{}'.format(10 * i, (10 * i) + 9))
            ages_range_calibration.append('80P')
            columns_final.append(self.create_columns_for_csv_with(names, ages_range_calibration))

            files_names.append('calibration_incidence')
            names = ['H1', 'H2', 'H3', 'ICU', 'D', 'R']  # Removed from Hospital only
            ages_range_calibration = []
            for i in range(0, 8):
                ages_range_calibration.append('{}-{}'.format(10 * i, (10 * i) + 9))
            ages_range_calibration.append('80P')
            columns_final.append(self.create_columns_for_csv_with(names, ages_range_calibration))

        return columns_final, files_names

    @staticmethod
    def create_columns_for_csv_with(columns_name, age_range):
        """
        Creates a pandas MultiIndex from columns and age ranges
        :param columns_name: list of columns name (often compartment name)
        :param age_range: list of age ranges
        :return: a pandas MultiIndex
        """
        first_header = ['']
        second_header = ['']
        for name in columns_name:
            for age in age_range:
                first_header.append(name)
                second_header.append(age)
        return pd.MultiIndex.from_tuples(zip(first_header, second_header))

    @staticmethod
    def infection_probability_update(previous_infection_probability, current_agent, contacted_agent):
        """
        Updates the previous infection probability with new information (Eikenberry's approach)
        :param previous_infection_probability:
        :param current_agent:
        :param contacted_agent:
        :return:
        """
        return previous_infection_probability

    @staticmethod
    def empty(*args):
        """
        Used as default empty function when simulation attributes are set to 0.
        :param args:
        :return:
        """
        return True

    def update_parameters(self, agent, attributes, local_agent_id, age_index=-1):
        """

        :param agent:
        :param attributes:
        :param local_agent_id:
        :param age_index:
        :return:
        """
        if GlobalVar.Instance().transition_type == "Weibull":
            agent.attributes['weibull_k'] = self.transition_type['k']
            for name, value in self.variants[self.default_variant].attributes['residence_time'].items():
                if type(value) == list:
                    lambda_ = value[age_index]
                else:
                    lambda_ = value
                agent.attributes[name] = lambda_

    @staticmethod
    def fetch_variant_parameters(agent, variant, age_index=None):
        """
        Fetch attributes related to a specific variant of the COVID.
        :param agent: current agent
        :param variant: current variant
        :param age_index:
        :return:
        """
        agent.variant_name = variant.name
        agent.attributes['variant'] = variant.name
        for attribute, value in variant.non_age_dependant_attributes.items():
            agent.attributes[attribute] = value
        if age_index is not None:
            for age_specific_attribute, value in variant.age_dependant_attributes.items():
                agent.attributes[age_specific_attribute] = value[age_index]
        # If agent has received a successful vaccine shot, some of its attributes shouldn't be updated.
        if 'success_vaccinated' in agent.attributes:
            GlobalVar.Instance().vaccines[agent.attributes['vaccine_received']].update_attributes(agent)

    @staticmethod
    def fetch_parameters_for(agent, parameters, age_index=None):
        if GlobalVar.Instance().tracing:
            for key, value in parameters['tracing_parameters'].items():
                agent.attributes[key] = value
        if GlobalVar.Instance().vaccination:
            vac_prm = parameters['vaccination_parameters']
            # TODO check behavior of vaccination at the start of the simulation
            if vac_prm['start_iteration'] == 0:
                # Vaccinated at the start of the simulation
                agent.attributes['is_vaccinated'] = True
                agent.attributes['first_shot'] = False
                agent.attributes['second_shot'] = False
                agent.attributes['vaccine_success'] = 1 if rnd.random() < vac_prm['success_rate'] else 0
            else:
                agent.attributes['is_vaccinated'] = False
                agent.attributes['first_shot'] = False
                agent.attributes['second_shot'] = False
                agent.attributes['vaccine_success'] = False  # Depends of the vaccine success rate

            if age_index is not None:
                agent.attributes['wants_vaccine'] = 1 if rnd.random() < vac_prm['wants_vaccine'] else 0
            else:
                agent.attributes['wants_vaccine'] = 1 if rnd.random() < vac_prm['wants_vaccine_per_age'][
                    age_index] else 0
                for priority, values in vac_prm['strategy'][vac_prm['vaccine_strategy']]['priority'].items():
                    if age_index in values['age'] and values['start'] == 0:
                        agent.attributes['is_vaccinated'] = True
                        agent.attributes['first_shot'] = False
                        agent.attributes['second_shot'] = False
                        agent.attributes['vaccine_success'] = 1 if rnd.random() < vac_prm['success_rate'] else 0

    def draw_contacts_for_agent(self, agent, number_of_potential_new_infection):
        """
        Describes how a model draws new possible infections
        :return:
        """
        pass

    def daily_contacts(self, agent, infection_probability):
        """
        For each contact, checks if an infectious occurs and store every contacts of the day.
        :param infection_probability:
        :param agent:
        :return: one list with newly infected contacts and one list of non-infectious contacts
        """
        newly_infected_agents, contacts_without_infection = [], []
        if self.infection_type == "uni_directional":
            # No contacts for the day
            newly_infected_agents, contacts_without_infection = self.directional_infection2(agent,
                                                                                            infection_probability,
                                                                                            [])
            self.uni_direction_contacts(agent, contacts_without_infection)
        elif self.infection_type == "bi_directional":
            newly_infected_agents, contacts_without_infection = self.directional_infection2(agent,
                                                                                            infection_probability,
                                                                                            agent.contacts[-1])
            self.bi_direction_contacts(agent, contacts_without_infection)
        return newly_infected_agents, contacts_without_infection

    @staticmethod
    def uni_direction_contacts(agent, contacts_without_infection):
        """
        Updates only the list of contacted agent.
        :param agent:
        :param contacts_without_infection:
        :return:
        """
        for contact_without_infection in contacts_without_infection:
            agent.contacts[-1].append(contact_without_infection.id)

    @staticmethod
    def bi_direction_contacts(agent, contacts_without_infection):
        """
        Updates the list of contacted agents and adds the current agent as contact for contacted agents.
        :param agent:
        :param contacts_without_infection:
        :return:
        """
        for contact_without_infection in contacts_without_infection:
            agent.contacts[-1].append(contact_without_infection.id)
            if contact_without_infection.contacts_day != agent.env.now:
                contact_without_infection.contacts.append([agent.id])
                contact_without_infection.contacts_day = agent.env.now
            else:
                contact_without_infection.contacts[-1].append(agent.id)

    def directional_infection(self, agent, population, infection_probability, contacts):
        pass

    @staticmethod
    def check_multiple_infections(newly_infected_agent, newly_infected_agents):
        """
        Checks if an agent was infected during this simulation step. Used when the
        parameter 'infection_only_susceptible' is True
        :param newly_infected_agent: drawn agent
        :param newly_infected_agents: list of agents to be infected later
        :return: remaining infections to perform. Either -1 or 0
        """
        if newly_infected_agent.compartment.name == 'susceptible':
            newly_infected_agent.just_infected = True
            newly_infected_agents.append(newly_infected_agent)
            return -1
        elif newly_infected_agent.just_infected:  # does not count as an infection attempt
            return 0
        else:  # an attempt on an already active agent
            return -1

    @staticmethod
    def check_multiple_infections_and_store(newly_infected_agent, newly_infected_agents, contacts_without_infection):
        """
        Checks if an agent was infected during this simulation step. Used when the
        parameter 'infection_only_susceptible' is True
        :param contacts_without_infection:
        :param newly_infected_agent: drawn agent
        :param newly_infected_agents: list of agents to be infected later
        :return: remaining infections to perform. Either -1 or 0
        """
        if newly_infected_agent.compartment.name == 'susceptible':
            newly_infected_agent.just_infected = True
            newly_infected_agents.append(newly_infected_agent)
            return -1
        elif newly_infected_agent.just_infected:  # does not count as an infection attempt
            return 0
        else:  # an attempt on an already active agent
            contacts_without_infection.append(newly_infected_agent)
            return -1

    def vaccine_distribution_based_on_proportion(self, agents, proportion_of_vaccines, iteration, age_index,
                                                 current_capacity):
        """
        Gives the vaccine to each agent to match the available proportion of vaccines.
        Considers three scenarios :
        1) only one vaccine available for this priority
        2) only one vaccine available for this specific age range within a priority with multiple vaccines
        3) two or more vaccines available for this specific age range where the sum of proportion should be equals to 1
        :param agents: list of non vaccinated agents of a given age
        :param proportion_of_vaccines: dict with a list of vaccines and their proportion per age
        :param iteration: current iteration (int)
        :param age_index: specified age_index, used in the proportion_of_vaccines dict
        :param current_capacity: available number of vaccines for the current iteration
        :return: updated current_capacity
        """
        multiple_vaccines = []  # list of vaccine names (str)
        proportion_vaccine = []  # list of cumulative sum of float, last element should be equals to 1
        if len(proportion_of_vaccines['vaccines']) == 1:
            # Only one vaccine
            current_vaccine = proportion_of_vaccines['vaccines'][0]
        else:
            # Two or more available vaccines
            if len(proportion_of_vaccines['age'][str(age_index)]) == 1:
                # only one vaccine for this age range
                current_vaccine = list(proportion_of_vaccines['age'][str(age_index)])[0]
            else:
                cum_sum = 0.0
                for vaccine_name, vaccine_proportion in proportion_of_vaccines['age'][str(age_index)].items():
                    multiple_vaccines.append(vaccine_name)
                    cum_sum += vaccine_proportion
                    proportion_vaccine.append(cum_sum)
                assert proportion_vaccine[-1] == 1.0
        if len(multiple_vaccines) > 0:
            for agent in agents:
                if agent.compartment.name not in self.vaccine_attributes['ignored_compartments']:
                    vacc_rnd = rnd.random()
                    for vaccine_index, proportion in enumerate(proportion_vaccine):
                        if vacc_rnd <= proportion:
                            GlobalVar.Instance().vaccines[multiple_vaccines[vaccine_index]].get_first_shot(agent,
                                                                                                           GlobalVar,
                                                                                                           iteration)
                            self.first_shot_agents[iteration].append(agent)
                            current_capacity -= 1
        else:
            for agent in agents:
                if agent.compartment.name not in self.vaccine_attributes['ignored_compartments']:
                    GlobalVar.Instance().vaccines[current_vaccine].get_first_shot(agent, GlobalVar,
                                                                                  iteration)
                    self.first_shot_agents[iteration].append(agent)
                    current_capacity -= 1
                # TODO add vaccine behavior for first and second injection
        return current_capacity

    def vaccine_distribution_new(self, iteration, population):
        """

        :param iteration:
        :param population:
        :return:
        """
        if iteration >= self.vaccine_attributes['start_iteration'] and len(self.vaccine_attributes['priority']) > 0:
            # checks current priority and updates it.
            for age_prio in self.vaccine_attributes['priority'][0]:
                # Removes age range if there is no more agent to vaccine in this age range
                if len(population.vaccine_population_per_age[age_prio]) == 0:
                    self.vaccine_attributes['priority'][0].remove(age_prio)

            # If there is no more agents to vaccine in this priority, removes it and updates priority
            if len(self.vaccine_attributes['priority'][0]) == 0:
                self.vaccine_attributes['priority'].pop(0)
                self.vaccine_attributes['vaccines_per_priority'].pop(0)
                if len(self.vaccine_attributes['priority']) == 0:
                    # Vaccination campaign is over
                    pass
                elif len(self.vaccine_attributes['priority']) == 1 and -1 in self.vaccine_attributes['priority']:
                    # Takes the default priority (remaining age range)
                    self.vaccine_attributes['priority'][0] = []
                    self.vaccine_attributes['vaccines_per_priority'][0] = \
                        self.vaccine_attributes['vaccines_per_priority'][-1]
                    # Put remaining age range to be vaccinated
                    for age_range, agents_per_age in enumerate(population.vaccine_population_per_age):
                        if len(agents_per_age) > 0:
                            self.vaccine_attributes['priority'][0].append(age_range)
                else:
                    # Updates priority with the next one
                    self.vaccine_attributes['priority'][0] = self.vaccine_attributes['priority'].pop(
                        min([value for value in self.vaccine_attributes['priority'].keys() if value != -1]))
                    self.vaccine_attributes['vaccines_per_priority'][0] = self.vaccine_attributes[
                        'vaccines_per_priority'].pop(
                        min([value for value in self.vaccine_attributes['vaccines_per_priority'].keys() if
                             value != -1]))

            if len(self.vaccine_attributes['priority']) > 0 and len(
                    self.vaccine_attributes['priority'][0]) > 0:  # old version: self.vaccine_attributes['priority']
                current_vaccines = self.vaccine_attributes['vaccines_per_priority'][0]
                # current_vaccines is a dict with two attributes: "age" which is a dict of age and proportion
                # of vaccines and "vaccines" which is a list of vaccines
                if len(current_vaccines['vaccines']) == 1:
                    current_vaccine = current_vaccines['vaccines'][0]
                else:
                    # More than one vaccine for the current priority
                    pass

                first_shot_number = GlobalVar.Instance().first_shots_per_iteration[iteration] + self.vaccine_attributes[
                    'remaining_vaccine']
                self.vaccine_attributes['remaining_vaccine'] = 0
                remaining_vaccines = first_shot_number % len(self.vaccine_attributes['priority'][0])
                self.vaccine_attributes['remaining_vaccine'] += remaining_vaccines

                available_vaccines_iter = first_shot_number - remaining_vaccines
                used_vaccines_iter = 0
                pop_age = GlobalVar.Instance().population_of_agents_per_age
                sum_pop = 0
                for age_range_ in self.vaccine_attributes['priority'][0]:
                    sum_pop += len(pop_age[age_range_])
                for age_range, agents_per_age in enumerate(population.vaccine_population_per_age):
                    if age_range in self.vaccine_attributes['priority'][0]:
                        percent_pop = len(pop_age[age_range]) * 100 / sum_pop
                        current_capacity = floor(percent_pop * available_vaccines_iter / 100)
                        used_vaccines_iter += current_capacity
                        # Computes current capacity of vaccine for this iteration
                        if current_capacity > 0:
                            # Gets non-vaccinated agents and remove them from the list
                            if current_capacity >= len(agents_per_age):
                                agents = population.vaccine_population_per_age[age_range]
                                population.vaccine_population_per_age[age_range] = []
                            else:
                                rnd.shuffle(population.vaccine_population_per_age[age_range])
                                agents = population.vaccine_population_per_age[age_range][:current_capacity]
                                del population.vaccine_population_per_age[age_range][:current_capacity]
                            current_capacity = self.vaccine_distribution_based_on_proportion(agents, current_vaccines,
                                                                                             iteration, age_range,
                                                                                             current_capacity)
                        self.vaccine_attributes['remaining_vaccine'] += current_capacity
                assert available_vaccines_iter >= used_vaccines_iter
                self.vaccine_attributes['remaining_vaccine'] += available_vaccines_iter - used_vaccines_iter

        for vaccine_name, vaccine in GlobalVar.Instance().vaccines.items():
            # Gives second shot
            time_injection = vaccine.attributes["iteration_before_next_injection"]
            if iteration >= self.vaccine_attributes['start_iteration'] + time_injection and len(
                    self.first_shot_agents[iteration - int(time_injection)]) > 0:
                for agent in self.first_shot_agents[iteration - int(time_injection)]:
                    if agent.attributes['vaccine_received'] == vaccine_name:
                        GlobalVar.Instance().vaccines[vaccine_name].get_second_shot(agent, GlobalVar)
                        self.second_shot_agents.append(agent)
            # Applies effects of vaccine
            for agent in GlobalVar.Instance().success_agents_vaccinated_per_iteration[iteration]:
                vaccine.update_attributes_for_injection(agent, "attributes_first_injection")
