#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By : Simon Pageaud
# License    : BSD-3-Clause Copyright 2021, CovDyn Developers.
# =============================================================================
"""
This module contains the Population class that stores every agents of the
simulation.
"""
# =============================================================================
# Imports
# =============================================================================
from covid_model import CovDynModel
from global_variables import GlobalVar, compute_successive_waves

from datetime import datetime
import collections
from numpy import *
from typing import NamedTuple, Any
import numpy as np


class CsvFile(NamedTuple):
    name: str
    lines: list
    columns: Any


class Population:

    def __init__(self, parameters):
        self.env = None
        self.parameters = parameters
        self.time_step = self.parameters['time_step']
        GlobalVar.Instance().time_step = self.time_step
        self.action = None

        self.age_distribution = np.floor(parameters['reduction_factor'] * np.asarray(parameters['population_per_age']))
        self.total_population = sum(self.age_distribution)

        self.number_agents_per_compartments = collections.OrderedDict()
        self.number_agents_per_compartments_per_age = collections.OrderedDict()

        self.compartment_names = self.parameters['compartment_names']

        if 'tracing' in self.parameters and self.parameters['tracing']:
            GlobalVar.Instance().tracing = True

        self.vaccine_population_per_age = []
        if 'vaccination' in self.parameters and self.parameters['vaccination']:
            GlobalVar.Instance().vaccination = True

        # COVID related attributes
        self.COVID_model_name = self.parameters['COVID_model']
        GlobalVar.Instance().COVID_model = self.COVID_model_name
        GlobalVar.Instance().COVID_model_object = self.create_model(parameters)

        # Adds new files
        self.files = {}  # Stores a dict of CsvFile to save as csv at the end of the simulation
        all_columns, all_names = GlobalVar.Instance().COVID_model_object.create_multiple_columns(
            calibration=parameters['calibration'])
        assert len(all_columns) == len(all_names)
        for index_of_file, name in enumerate(all_names):
            self.files[name] = CsvFile(name, [], all_columns[index_of_file])
        self.files['evo_pop'] = CsvFile('evo_pop', [], GlobalVar.Instance().COVID_model_object.create_columns_for_csv())

        self.variants = self.parameters['variant']

        # Stores IDs of agents per compartment
        self.population_per_compartments = collections.OrderedDict()
        # Stores IDs of agents per compartment and per age range
        self.population_per_compartments_per_age = collections.OrderedDict()

        # Initialization of compartment related structures
        for compartment_name in self.compartment_names:
            self.number_agents_per_compartments[compartment_name] = 0
            self.number_agents_per_compartments_per_age[compartment_name] = []
            GlobalVar.Instance().number_agents_per_compartments_per_age_per_variant[compartment_name] = {}
            GlobalVar.Instance().agents_in_per_comp_age_variant_iter[compartment_name] = {}
            GlobalVar.Instance().agents_out_per_comp_age_variant_iter[compartment_name] = {}
            if compartment_name in ['hospitalized', 'hospitalizedSecond', 'hospitalizedThird', 'ICU']:
                GlobalVar.Instance().cumulated_agents_age_variant[compartment_name] = {}
            for variant_name, variant in GlobalVar.Instance().variants.items():
                GlobalVar.Instance().number_agents_per_compartments_per_age_per_variant[compartment_name][
                    variant_name] = []
                GlobalVar.Instance().agents_in_per_comp_age_variant_iter[compartment_name][variant_name] = []
                GlobalVar.Instance().agents_out_per_comp_age_variant_iter[compartment_name][variant_name] = []
                if compartment_name in ['hospitalized', 'hospitalizedSecond', 'hospitalizedThird', 'ICU']:
                    GlobalVar.Instance().cumulated_agents_age_variant[compartment_name][variant_name] = []

        self.population_of_agents = []  # Picks a random agent from this during an infection
        self.population_of_agents_per_age = []
        # Used to create other initial compartments by randomly picking agents from the susceptible compartment
        self.susceptible = []
        self.susceptible_per_age = []
        self.total_susceptible_per_age = []

        self.agents = GlobalVar.Instance().COVID_model_object.create_population(parameters, self)
        GlobalVar.Instance().COVID_model_object.create_compartments(parameters, self)
        GlobalVar.Instance().number_agents_per_compartments = self.number_agents_per_compartments
        GlobalVar.Instance().number_agents_per_compartments_per_age = self.number_agents_per_compartments_per_age

        self.folder_name = GlobalVar.Instance().folder_name
        self.filename = self.COVID_model_name + datetime.now().isoformat().split('.')[0] + '.txt'
        self.filename = self.filename.replace(':', '_')

        # counter for attributes update
        self.attributes_update_counter = 0

        self.vaccine_behavior = GlobalVar.Instance().COVID_model_object.vaccine_distribution_new if \
            GlobalVar.Instance().vaccination else self.empty

        GlobalVar.Instance().beta2, GlobalVar.Instance().R_per_iteration = compute_successive_waves(
            self.parameters)
        GlobalVar.Instance().population_of_agents_per_age = self.population_of_agents_per_age

    def add_environment(self, env):
        self.env = env
        GlobalVar.Instance().env = env
        self.action = env.process(self.live())
        for agent in self.agents:
            agent.add_environment(self.env)
            agent.action = agent.env.process(agent.live())

    def reset_agents(self, initial_values):
        assert len(initial_values) == len(self.agents)
        for index, agent in enumerate(self.agents):
            agent.reset_for_calibration(initial_values[str(index)])

    def create_model(self, parameters):
        """
        Creates a specify model with the given dictionary of attributes.
        :param parameters: a dictionary of attributes
        :return: the specified COVID_model
        """
        if self.COVID_model_name == 'CovDyn_Model':
            return CovDynModel(parameters)
        else:
            assert 0, "Wrong model name."

    def wrap_up(self):
        """
        This method is called at the end of the simulation to display, log or save specific information.
        :return:
        """

    def compute_updated_beta2(self):
        """
        Variant of the simulation with a beta2 computes as a effective reproduction number but only when R0 change
        instead of every iteration
        :return:
        """
        sanitary_update = self.parameters['sanitary_update']
        changes = []
        for section in sanitary_update:
            changes.append(int(sanitary_update[section]['T_b'] / self.time_step))
        if self.env.now + self.time_step in changes:
            r_0 = self.parameters['R0_base4dynamic_Re']
            beta2 = GlobalVar.Instance().R_per_iteration[GlobalVar.Instance().env.now + self.time_step] / r_0 * \
                    self.total_population / GlobalVar.Instance().number_agents_per_compartments["susceptible"]

            updated_beta2 = np.ones(len(GlobalVar.Instance().beta2)) * beta2
            GlobalVar.Instance().beta2 = updated_beta2

    def update_line_for_csv_file(self):
        for file_name, file in self.files.items():
            file.lines.append([GlobalVar.Instance().clock])
            file.lines[self.env.now].extend(GlobalVar.Instance().COVID_model_object.live(file_name))

    def empty(self, *args):
        """
        Empty function used when parameters are set (testing, tracing, ...).
        """
        pass

    def add_new_variant(self):
        """
        Adds a new variant during simulation based on 'start' value from parameters
        :return:
        """
        for name, variant in GlobalVar.Instance().variants.items():
            if self.env.now == variant.start / self.time_step:
                # When the 'start' value matches the current simulation iteration
                variant.initialize_variant_during_simulation(
                    GlobalVar.Instance().COVID_model_object, self.parameters, self)

    def update_attributes_at_iteration(self):
        """
        Checks if the current iteration matches an iteration for attributes update
        """
        for iteration in self.parameters['attributes_update']['iterations']:
            if self.env.now == iteration / self.time_step:
                for name, variant in GlobalVar.Instance().variants.items():
                    # For each variant, updates its parameters
                    variant.update_attributes_from_parameters(
                        self.parameters['attributes_update'][str(self.attributes_update_counter)])
                # Updates attributes in each agent
                for agent in self.agents:
                    if agent.variant_name is not None:
                        GlobalVar.Instance().COVID_model_object. \
                            fetch_variant_parameters(agent,
                                                     GlobalVar.Instance().variants[agent.variant_name],
                                                     agent.age_id)
                self.attributes_update_counter += 1

    @staticmethod
    def reset_day():
        """
        Resets global variables to 0 at the end of the day
        :return:
        """
        for compartment, variants in GlobalVar.Instance().agents_out_per_comp_age_variant_iter.items():
            for variant, ages in variants.items():
                for age_range, agents_in_age_range in enumerate(ages):
                    GlobalVar.Instance().agents_out_per_comp_age_variant_iter[compartment][variant][age_range] = 0
                    GlobalVar.Instance().agents_in_per_comp_age_variant_iter[compartment][variant][age_range] = 0
        GlobalVar.Instance().new_entries_ICU = 0
        GlobalVar.Instance().alive_agents_iter = 0

    def live(self):
        """
        Stores the evolution of compartments at each simulation iteration and increments internal clock.
        """
        while True:
            self.vaccine_behavior(self.env.now, self)
            self.add_new_variant()
            self.update_attributes_at_iteration()
            self.update_line_for_csv_file()
            GlobalVar.Instance().update_clock()
            self.reset_day()
            yield self.env.timeout(1)
