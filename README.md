# CovDyn Agent-Based Model

The CovDyn Agent-Based Model is a simulator coded in python. 
This simulator aims to predict the course of the COVID-19 epidemic over an 18-month period starting on 1 January 2021 by
applying several combinations of vaccination campaigns and barrier measures and, taking into account the increasing 
proportion of the Alpha variant.

The simulation used data from [Polymod and INSEE studies](https://lwillem.shinyapps.io/socrates_rshiny/) 
to create daily contact matrix per age and to describe average daily contacts.

## Installation

The simulation uses the [simpy library](https://simpy.readthedocs.io/en/latest/).
```bash
pip install simpy pandas numpy scipy
```

## Usage

To launch a simulation:
```bash
python simulation_generator.py -f name_of_scenario
```

Where the ```name_of_scenario``` is one of the three available scenario:
* ```historical_strain_without_vaccination``` Epidemic with the historical strain without vaccination
* ```historical_variant_strains_without_vaccination``` Epidemic with the historical strain and the Alpha variant without vaccination
* ```historical_variant_strains_with_vaccination``` Epidemic with the historical strain and the Alpha variant with vaccination


Finally, a word of caution: the CovDyn_ABM requires a large amount of RAM to simulate all agents.